var ClearedLayer = cc.LayerColor.extend({
	killRecord:-1,

	initWithTimeRecord:function (color, killRecord) {
		if(!this.init(color))
		{
			return false;
		}

		// save data
		this.killRecord = killRecord;

		// fade in backdrop
		var sequence = cc.Sequence.create(
			cc.FadeTo.create(1.0, 128),
			cc.CallFunc.create(this.showUIElements, this)
		);
		this.runAction(sequence);

		return true;
	},
	showUIElements:function() {
		var winSize = cc.Director.getInstance().getWinSize();

		// labels
		var youDieLabel = cc.LabelTTF.create("Game Over", "AtariClassic", 40);
		youDieLabel.setColor(cc.c3b(255,0,0));
		youDieLabel.setPosition(
			cc.p(winSize.width/2,
				 winSize.height/1.5 - youDieLabel.getContentSize().height/2)
			);
		this.addChild(youDieLabel);

		var waveLabel = cc.LabelTTF.create("Kill Record: " + this.killRecord, "AtariClassic", 18);
		waveLabel.setColor(cc.c3b(255,255,255));
		waveLabel.setPosition(
			cc.p(
				winSize.width/2,
				youDieLabel.getPositionY() - youDieLabel.getContentSize().height/2 - 20 - waveLabel.getContentSize().height/2)
			);
		this.addChild(waveLabel);

		var restartGameLabel = cc.LabelTTF.create("Press x to continue", "AtariClassic", 20);
		restartGameLabel.setColor(cc.c3b(255,255,255));
		restartGameLabel.setPosition(
			cc.p(
				winSize.width/2, 
				winSize.height/4.5));
		var blink = cc.Blink.create(2.0, 3);
		var forever = cc.RepeatForever.create(blink);
		restartGameLabel.runAction(forever);
		this.addChild(restartGameLabel);

		this.setKeyboardEnabled(true);
	},
	onKeyUp:function (e) {
		// nothing here
	},
	onKeyDown:function (e) {
		if(e == cc.KEY.x)
		{
			this.getParent().removeAllChildren();

			// go back to main menu
			cc.Director.getInstance().replaceScene(new MainMenuScene());
		}
	}
});

ClearedLayer.create = function(killRecord) {
	var layer = new ClearedLayer();
	if(layer && layer.initWithTimeRecord(cc.c4b(30, 30, 30, 30), killRecord))
	{
		return layer;
	}
	else
	{
		layer = null;
		return null;
	}
}