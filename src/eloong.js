var EloongState = {
	IDLE_NORMAL_STATE: 1,
	RUN_NORMAL_STATE: 2,
	ATTACK_NORMAL_STATE: 3,
	DIE_STATE: 4,
	BEING_ATTACK_STATE: 5,
	JUMP_NORMAL_STATE: 6,

	IDLE_HYPER_STATE: 7,
	RUN_HYPER_STATE: 8,
	ATTACK_HYPER_STATE: 9,
	ATTACKRUN_HYPER_STATE: 10,
	JUMP_HYPER_STATE: 11,

	IDLE_TIRED_STATE: 12,
	RUN_TIRED_STATE: 13,
	ATTACK_TIRED_STATE: 14,
	JUMP_TIRED_STATE: 15
}

var EloongAnimationTag = {
	ACTIONS: 1,
	MOVEMENT: 2
}

var EloongMode = {
	NORMAL:1,
	HYPER:2,
	TIRED:3
}
var EloongAttributes = {
	SPEED_NORMAL_X: 10,
	SPEED_NORMAL_Y: 10,
	JUMP_NORMAL_SPEED_X: 40,
	JUMP_NORMAL_SPEED_Y: 60,

	SPEED_HYPER_X: 40,
	SPEED_HYPER_Y: 10,
	JUMP_HYPER_SPEED_X: 80,
	JUMP_HYPER_SPEED_Y: 60,

	SPEED_TIRED_X: 10,
	SPEED_TIRED_Y: 10,
	JUMP_TIRED_SPEED_X: 40,
	JUMP_TIRED_SPEED_Y: 60,

	THRESHOLD_ACTIVATE_ATTACKRUN_HYPER: 0.6,  // in seconds
	THRESHOLD_TO_CHANGE_TO_HYPER: 30
}
var Direction = {
	LEFT: 1,
	RIGHT: 2
}

var Eloong = cc.Sprite.extend({
	_parentNode:null,
	_countingTimeAttackRunHyper:0,
	_isAttackRunHyperStartedToCount:false,
	_isProhibitedToPlayAttackHyper:false,
	
	// universal
	_dieAnim:null,
	_beingAttackAnim:null,
	// normal
	_idleNormalAnim:null,
	_runNormalAnim:null,
	_attackNormalAnim:null,
	_jumpNormalAnim:null,
	// hyper
	_idleHyperAnim:null,
	_runHyperAnim:null,
	_attackHyperAnim:null,
	_attackRunHyperAnim:null,
	_jumpHyperAnim:null,

	// tired
	_idleTiredAnim:null,
	_runTiredAnim:null,
	_attackTiredAnim:null,
	_jumpTiredAnim:null,

	// _flags
	_isDownKeyPressed:false,

	// -- hyperFire sprite
	_hyperFire:null,
	_hyperFireBatch:null,

	vy: 3,
	currentState:null,
	currentMode:EloongMode.NORMAL,
	facingDirection: Direction.Left,
	vy: 7,

	initWithIdleFrame:function (parentNode) {
		this._parentNode = parentNode;

		// load eloong spritesheet
		global.loadSpriteFrames(res_eloongSpritesheetPlist);

		if(!this.initWithSpriteFrame(global.getSpriteFrame("eloong_idle_1.png")))
		{
			global.log("Eloong's initWithSpace() error.");
			// unload eloong spritesheet
			global.unloadSpriteFrames(res_eloongSpritesheetPlist);

			return false;
		}

		// init stuff
		this._countingTimeAttackRunHyper = 0;
		this._isProhibitedToPlayAttackHyper = false;
		this._isAttackRunHyperStartedToCount = false;
		this.currentMode = EloongMode.NORMAL;
		this.facingDirection = Direction.LEFT;
		this._isDownKeyPressed = false;

		// create animations
		// ## 1 #####
		// # Normal #
		// ##########
		var frames = new Array();
		var animate = null;

		// 1.1 Idle
		frames.push(global.getSpriteFrame("eloong_idle_1.png"));
		frames.push(global.getSpriteFrame("eloong_idle_2.png"));
		animate = cc.Animate.create(cc.Animation.create(frames, 1/7.0));
		this._idleNormalAnim = cc.RepeatForever.create(animate);
		//this._idleNormalAnim.setTag(EloongAnimationTag.ACTIONS);

		// 1.2 Run (in fact, it's walk)
		frames.length = 0;
		frames.push(global.getSpriteFrame("eloong_walk_1.png"));
		frames.push(global.getSpriteFrame("eloong_walk_2.png"));
		frames.push(global.getSpriteFrame("eloong_walk_3.png"));
		frames.push(global.getSpriteFrame("eloong_walk_4.png"));
		animate = cc.Animate.create(cc.Animation.create(frames, 1/7.0));
		this._runNormalAnim = cc.RepeatForever.create(animate);
		//this._runNormalAnim.setTag(EloongAnimationTag.ACTIONS);

		// 1.3 Attack
		frames.length = 0;
		frames.push(global.getSpriteFrame("eloong_attack_1.png"));
		frames.push(global.getSpriteFrame("eloong_attack_2.png"));
		animate = cc.Animate.create(cc.Animation.create(frames, 1/14.0));
		this._attackNormalAnim = cc.Repeat.create(animate, 1);
		//this._attackNormalAnim.setTag(EloongAnimationTag.ACTIONS);

		// 1.4 Die
		frames.length = 0;
		frames.push(global.getSpriteFrame("eloong_die.png"));
		animate = cc.Animate.create(cc.Animation.create(frames, 1/7.0));
		this._dieAnim = cc.Repeat.create(animate, 1);

		// 1.5 Being attack
		frames.length = 0;
		frames.push(global.getSpriteFrame("eloong_being_attacked.png"));
		animate = cc.Animate.create(cc.Animation.create(frames, 1/7.0));
		this._beingAttackAnim = cc.Repeat.create(animate, 1);

		// 1.6 Jump
		frames.length = 0;
		frames.push(global.getSpriteFrame("eloong_jump.png"));
		animate = cc.Animate.create(cc.Animation.create(frames, 1/7.0));
		this._jumpNormalAnim = cc.Repeat.create(animate, 1);

		// ## 2 ####
		// # Hyper #
		// #########
		// 2.1 Idle
		frames.length = 0;
		frames.push(global.getSpriteFrame("eloong_hyper_idle_1.png"));
		frames.push(global.getSpriteFrame("eloong_hyper_idle_2.png"));
		animate = cc.Animate.create(cc.Animation.create(frames, 1/7.0));
		this._idleHyperAnim = cc.RepeatForever.create(animate);

		// 2.2 Run
		frames.length = 0;
		frames.push(global.getSpriteFrame("eloong_hyper_run_1.png"));
		frames.push(global.getSpriteFrame("eloong_hyper_run_2.png"));
		frames.push(global.getSpriteFrame("eloong_hyper_run_3.png"));
		frames.push(global.getSpriteFrame("eloong_hyper_run_4.png"));
		animate = cc.Animate.create(cc.Animation.create(frames, 1/12.0));
		this._runHyperAnim = cc.RepeatForever.create(animate);

		// 2.3 Attack
		frames.length = 0;
		frames.push(global.getSpriteFrame("eloong_hyper_attack_1.png"));
		frames.push(global.getSpriteFrame("eloong_hyper_attack_2.png"));
		frames.push(global.getSpriteFrame("eloong_hyper_attack_3.png"));
		animate = cc.Animate.create(cc.Animation.create(frames, 1/7.0));
		this._attackHyperAnim = cc.Repeat.create(animate, 1);

		// 2.4 Attack-Run
		frames.length = 0;
		frames.push(global.getSpriteFrame("eloong_hyper_run_attack_1.png"));
		frames.push(global.getSpriteFrame("eloong_hyper_run_attack_2.png"));
		frames.push(global.getSpriteFrame("eloong_hyper_run_attack_3.png"));
		frames.push(global.getSpriteFrame("eloong_hyper_run_attack_4.png"));
		animate = cc.Animate.create(cc.Animation.create(frames, 1/7.0));
		this._attackRunHyperAnim = cc.Repeat.create(animate, 1);

		// 2.5 Jump
		frames.length = 0;
		frames.push(global.getSpriteFrame("eloong_hyper_jump.png"));
		animate = cc.Animate.create(cc.Animation.create(frames, 1/7.0));
		this._jumpHyperAnim = cc.Repeat.create(animate, 1);

		// ## 3 ####
		// # Tired #
		// #########
		// 3.1 Idle
		frames.length = 0;
		frames.push(global.getSpriteFrame("eloong_tired_idle_1.png"));
		frames.push(global.getSpriteFrame("eloong_tired_idle_2.png"));
		animate = cc.Animate.create(cc.Animation.create(frames, 1/3.0));
		this._idleTiredAnim = cc.RepeatForever.create(animate);

		// 3.2 Run
		frames.length = 0;
		frames.push(global.getSpriteFrame("eloong_tired_walk_1.png"));
		frames.push(global.getSpriteFrame("eloong_tired_walk_2.png"));
		frames.push(global.getSpriteFrame("eloong_tired_walk_3.png"));
		frames.push(global.getSpriteFrame("eloong_tired_walk_4.png"));
		animate = cc.Animate.create(cc.Animation.create(frames, 1/3.0));
		this._runTiredAnim = cc.RepeatForever.create(animate);

		// 3.3 Attack
		frames.length = 0;
		frames.push(global.getSpriteFrame("eloong_tired_attack_1.png"));
		frames.push(global.getSpriteFrame("eloong_tired_attack_2.png"));
		animate = cc.Animate.create(cc.Animation.create(frames, 1/4.0));
		this._attackTiredAnim = cc.Repeat.create(animate, 1);

		// 3.4 Jump
		frames.length = 0;
		frames.push(global.getSpriteFrame("eloong_tired_jump.png"));
		animate = cc.Animate.create(cc.Animation.create(frames, 1/7.0));
		this._jumpTiredAnim = cc.Repeat.create(animate, 1);

		// unload eloong spritesheet
		global.unloadSpriteFrames(res_eloongSpritesheetPlist);

		// -- load eloong's hyper fire
		global.loadSpriteFrames(res_eloongFirePlist);
		this._eloongHyperFire = cc.Sprite.createWithSpriteFrameName("eloong_fire_1.png");
		this._parentNode.addChild(this._eloongHyperFire, GameLayer.CHARACTER_LAYER+1);
		// -- batch node
		this._eloongHyperFireBatch = cc.SpriteBatchNode.create(res_eloongFire_1);
		this._parentNode.addChild(this._eloongHyperFireBatch, GameLayer.CHARACTER_LAYER+1);
		// -- eloong's fire animation
		var eloongFireFrames = [];
		var eloongFireAnimation = null;
		eloongFireFrames.push(cc.SpriteFrameCache.getInstance().getSpriteFrame("eloong_fire_1.png"));
		eloongFireFrames.push(cc.SpriteFrameCache.getInstance().getSpriteFrame("eloong_fire_2.png"));
		eloongFireFrames.push(cc.SpriteFrameCache.getInstance().getSpriteFrame("eloong_fire_3.png"));
		var eloongFireAnimation = cc.Animation.create(eloongFireFrames, 1/8);
		this._eloongHyperFire.setOpacity(0);
		this._eloongHyperFire.runAction(cc.RepeatForever.create(cc.Animate.create(eloongFireAnimation)));
		// unload eloongHyperFire spritesheet
		global.unloadSpriteFrames(res_eloongFirePlist);

		// start with idle-normal animation
		this.playIdleNormalAnimation();
		this.scheduleUpdate();
		this.schedule(this.updateEloongInfo, 0.10);

		return true;
	},
	// -- normal animation playing methods --
	playIdleNormalAnimation:function() {
		if(this.currentState != EloongState.IDLE_NORMAL_STATE)
		{
			this.stopAllActions();

			this.currentState = EloongState.IDLE_NORMAL_STATE;
			this.runAction(this._idleNormalAnim);
		}
	},
	playRunNormalAnimation:function() {
		//if(this.currentState != ElongState.RUN_NORMAL_STATE)
		//{
			this.stopAllActions();

			this.currentState = EloongState.RUN_NORMAL_STATE;
			this.runAction(this._runNormalAnim);
		//}
	},
	playAttackNormalAnimation:function() {
		if(this.currentState != EloongState.ATTACK_NORMAL_STATE)
		{
			this.stopAllActions();

			this.currentState = EloongState.ATTACK_NORMAL_STATE;
			this.runAction(
				cc.Sequence.create(
					this._attackNormalAnim,
					cc.CallFunc.create(this.playIdleNormalAnimation, this)
				)
			);
		}
	},
	playDieAnimation:function() {
		if(this.currentState != EloongState.DIE_STATE)
		{
			this.stopAllActions();

			this.currentState = EloongState.DIE_STATE;
			this.runAction(this._dieAnim);

			// set the game scene's isGameOver to true (parentNode)
			this._parentNode.isGameOver = true;
		}
	},
	playBeingAttackAnimation:function() {
		if((this.currentMode == EloongMode.NORMAL ||
		   this.currentMode == EloongMode.TIRED) && this.currentState != EloongState.BEING_ATTACK_STATE)
		{
			this.stopAllActions();
			audioEngine.playEffect(res_sfxHit);
			audioEngine.playEffect(res_sfxKikiHit);
			this.currentState = EloongState.BEING_ATTACK_STATE;
			this.runAction(
				cc.Sequence.create(
					this._beingAttackAnim,
					cc.CallFunc.create(this.playIdleAnimationForCurrentMode, this)
				)
			);
		}
	},
	playIdleAnimationForCurrentMode:function() {
		if(this.currentMode == EloongMode.NORMAL)
		{
			this.playIdleNormalAnimation();
		}
		else if(this.currentMode == EloongMode.HYPER)
		{
			this.playIdleHyperAnimation();
		}
		else if(this.currentMode == EloongMode.TIRED)
		{
			this.playIdleTiredAnimation();
		}
	},
	playJumpNormalAnimation:function() {
		this.stopAllActions();

		this.currentState = EloongState.JUMP_NORMAL_STATE;
		this.runAction(
			cc.Spawn.create(
				this._jumpNormalAnim,
				cc.Sequence.create(
					cc.JumpTo.create(1.0, cc.p(this.facingDirection == Direction.LEFT ? -EloongAttributes.JUMP_NORMAL_SPEED_X + this.getPositionX(): EloongAttributes.JUMP_NORMAL_SPEED_X + this.getPositionX(),this.getPositionY() + 120), EloongAttributes.JUMP_NORMAL_SPEED_Y, 1), 
					cc.CallFunc.create(this.playIdleNormalAnimation, this)
				)
			)
		);
	},
	playJumpDownNormalAnimation:function() {
		this.stopAllActions();

		this.currentState = EloongState.JUMP_NORMAL_STATE;
		this.runAction(
			cc.Spawn.create(
				this._jumpNormalAnim,
				cc.Sequence.create(
					cc.JumpTo.create(0.6, cc.p(this.getPositionX(),this.getPositionY() - 120), 10, 1), 
					cc.CallFunc.create(this.playIdleNormalAnimation, this)
				)
			)
		);
		audioEngine.playEffect(res_sfxLanding);
	},

	// -- hyper animation playing methods --
	playIdleHyperAnimation:function() {
		if(this.currentState != EloongState.IDLE_HYPER_STATE)
		{
			this.stopAllActions();

			this.currentState = EloongState.IDLE_HYPER_STATE;
			this.runAction(this._idleHyperAnim);
		}
	},
	playRunHyperAnimation:function() {
		//if(this.currentState != EloongState.RUN_HYPER_STATE)
		//{
			this.stopAllActions();

			this.currentState = EloongState.RUN_HYPER_STATE;
			this.runAction(this._runHyperAnim);
		//}
	},
	playAttackHyperAnimation:function() {
		if(this.currentState != EloongState.ATTACK_HYPER_STATE)
		{
			this.stopAllActions();

			this.currentState = EloongState.ATTACK_HYPER_STATE;
			this.runAction(
				cc.Sequence.create(
					this._attackHyperAnim,
					cc.CallFunc.create(this.playIdleHyperAnimation, this)
				)
			);
		}
	},
	playAttackRunHyperAnimation:function() {
		if(this.currentState != EloongState.ATTACKRUN_HYPER_STATE)
		{
			this.stopAllActions();

			this.currentState = EloongState.ATTACKRUN_HYPER_STATE;
			this.runAction(
				cc.Sequence.create(
					this._attackRunHyperAnim,
					cc.CallFunc.create(this.playIdleHyperAnimation, this)
				)
			);
		}
	},
	playJumpHyperAnimation:function() {
		this.stopAllActions();

		this.currentState = EloongState.JUMP_HYPER_STATE;
		this.runAction(
			cc.Spawn.create(
				this._jumpHyperAnim,
				cc.Sequence.create(
					cc.JumpTo.create(1.0, cc.p(this.facingDirection == Direction.LEFT ? -EloongAttributes.JUMP_HYPER_SPEED_X + this.getPositionX() : EloongAttributes.JUMP_HYPER_SPEED_X + this.getPositionX(),this.getPositionY() + 250), EloongAttributes.JUMP_HYPER_SPEED_Y, 1), 
					cc.CallFunc.create(this.playIdleHyperAnimation, this)
				)
			)
		);
	},
	playJumpDownHyperAnimation:function() {
		this.stopAllActions();

		this.currentState = EloongState.JUMP_HYPER_STATE;
		this.runAction(
			cc.Spawn.create(
				this._jumpHyperAnim,
				cc.Sequence.create(
					cc.JumpTo.create(0.6, cc.p(this.getPositionX(),this.getPositionY() - 120), 10, 1), 
					cc.CallFunc.create(this.playIdleHyperAnimation, this)
				)
			)
		);
	},

	// -- tired animation playing methods --
	playIdleTiredAnimation:function() {
		if(this.currentState != EloongState.IDLE_TIRED_STATE)
		{
			this.stopAllActions();

			this.currentState = EloongState.IDLE_TIRED_STATE;
			this.runAction(this._idleTiredAnim);
		}
	},
	playRunTiredAnimation:function() {
		//if(this.currentState != EloongState.RUN_TIRED_STATE)
		//{
			this.stopAllActions();

			this.currentState = EloongState.RUN_TIRED_STATE;
			this.runAction(this._runTiredAnim);
		//}
	},
	playAttackTiredAnimation:function() {
		if(this.currentState != EloongState.ATTACK_TIRED_STATE)
		{
			this.stopAllActions();

			this.currentState = EloongState.ATTACK_TIRED_STATE;
			this.runAction(
				cc.Sequence.create(
					this._attackNormalAnim,
					cc.CallFunc.create(this.playIdleTiredAnimation, this)
				)
			);
		}
	},
	playJumpTiredAnimation:function() {
		this.stopAllActions();

		this.currentState = EloongState.JUMP_TIRED_STATE;
		this.runAction(
			cc.Spawn.create(
				this._jumpTiredAnim,
				cc.Sequence.create(
					cc.JumpBy.create(1.0, cc.p(this.facingDirection == Direction.LEFT ? -EloongAttributes.JUMP_TIRED_SPEED_X : EloongAttributes.JUMP_TIRED_SPEED_X,0), EloongAttributes.JUMP_TIRED_SPEED_Y, 1), 
					cc.CallFunc.create(this.playIdleTiredAnimation, this)
				)
			)
		);
	},
	playJumpDownTiredAnimation:function() {
		this.stopAllActions();

		this.currentState = EloongState.JUMP_TIRED_STATE;
		this.runAction(
			cc.Spawn.create(
				this._jumpTiredAnim,
				cc.Sequence.create(
					cc.JumpTo.create(0.6, cc.p(this.getPositionX(),this.getPositionY() - 120), 10, 1), 
					cc.CallFunc.create(this.playIdleTiredAnimation, this)
				)
			)
		);
	},

	// -- keyboard --
	onKeyUp:function(e){
		if((e == cc.KEY.left || e == cc.KEY.right) &&
		   (this.currentState == EloongState.RUN_NORMAL_STATE ||
		   this.currentState == EloongState.RUN_HYPER_STATE ||
		   this.currentState == EloongState.RUN_TIRED_STATE))
		{
			this.playIdleAnimationForCurrentMode();
		}
		if(e == cc.KEY.down)
		{
			this._isDownKeyPressed = false;
		}

		// clear up things
		this._countingTimeAttackRunHyper = 0;
		this._isAttackRunHyperStartedToCount = false;
		this._isProhibitedToPlayAttackHyper = false;
	},
	onKeyDown:function(e) {
		// global.log(this.getPositionX()+":"+this.getPositionY());
		if(e == cc.KEY.v) {
			if(this.currentMode == EloongMode.NORMAL) {
				this.playAttackNormalAnimation();
				audioEngine.playEffect(res_gamePunchLight);
			}
			else if(this.currentMode == EloongMode.TIRED) {
				this.playAttackTiredAnimation();
				audioEngine.playEffect(res_gamePunchLight);
			}
			else if(this.currentMode == EloongMode.HYPER && this._countingTimeAttackRunHyper >= EloongAttributes.THRESHOLD_ACTIVATE_ATTACKRUN_HYPER)
			{
				// clear tracking states
				this._countingTimeAttackRunHyper = 0;
				this._isAttackRunHyperStartedToCount = false;

				// prohibit for one time to play attack-hyper
				this._isProhibitedToPlayAttackHyper = true;
				this.playAttackRunHyperAnimation();
				// -- play sfx
				audioEngine.playEffect(res_gamePunchHeavy);

				this._parentNode.getParent()._UI.decreaseStamina();
			}
			else if(this.currentMode == EloongMode.HYPER && !this._isProhibitedToPlayAttackHyper) 
			{
				this.playAttackHyperAnimation();
				audioEngine.playEffect(res_gamePunchHeavy);

				this._parentNode.getParent()._UI.decreaseStamina();
			}

			// check against all kikis
			for(var i=0; i<this._parentNode._kikis.length; i++)
			{
				var kiki = this._parentNode._kikis[i];
				if(kiki.hp <= 0)
					continue;

				var eloongRect = cc.RectMake(
					this.getPositionX()-this.getContentSize().width/2,
					this.getPositionY()-this.getContentSize().height/2,
					this.getContentSize().width,
					this.getContentSize().height);
				var kikiRect = cc.RectMake(
					kiki.getPositionX()-this.getContentSize().width/2,
					kiki.getPositionY()-this.getContentSize().height/2,
					kiki.getContentSize().width,
					kiki.getContentSize().height
					);

				if(cc.rectIntersectsRect(eloongRect, kikiRect))
				{
					var hitPoint = 1;

					if(this.currentMode == EloongMode.HYPER)
						hitPoint = 3;

					kiki.BeingHit(hitPoint);
					audioEngine.playEffect(res_sfxKikiHit);
					audioEngine.playEffect(res_sfxHit);

					break;
				}
			}
		}
		// Run - normal
		else if(e == cc.KEY.left && this.currentState != EloongState.RUN_NORMAL_STATE && this.isOkayForPlayingNewAnimation() && this.currentMode == EloongMode.NORMAL) {
			this.facingDirection = Direction.LEFT;
			this.setFlipX(false);
			this.playRunNormalAnimation();

			this.runAction(cc.RepeatForever.create(cc.MoveBy.create(0.1, cc.p(-EloongAttributes.SPEED_NORMAL_X, 0))));
		}
		else if(e == cc.KEY.right && this.currentState != EloongState.RUN_NORMAL_STATE && this.isOkayForPlayingNewAnimation() && this.currentMode == EloongMode.NORMAL) {
			this.facingDirection = Direction.RIGHT;
			this.setFlipX(true);
			this.playRunNormalAnimation();

			this.runAction(cc.RepeatForever.create(cc.MoveBy.create(0.1, cc.p(EloongAttributes.SPEED_NORMAL_X, 0))));
		}
		// Run - hyper
		else if(e == cc.KEY.left && this.currentState != EloongState.RUN_HYPER_STATE  && this.isOkayForPlayingNewAnimation() && this.currentMode == EloongMode.HYPER) {
			this.facingDirection = Direction.LEFT;
			this.setFlipX(false);
			this.playRunHyperAnimation();

			this.runAction(cc.RepeatForever.create(cc.MoveBy.create(0.1, cc.p(-EloongAttributes.SPEED_HYPER_X, 0))));

			this._isAttackRunHyperStartedToCount = true;

			this._parentNode.getParent()._UI.decreaseStamina();
		}
		else if(e == cc.KEY.right && this.currentState != EloongState.RUN_HYPER_STATE && this.isOkayForPlayingNewAnimation() && this.currentMode == EloongMode.HYPER) {
			this.facingDirection = Direction.RIGHT;
			this.setFlipX(true);
			this.playRunHyperAnimation();

			this.runAction(cc.RepeatForever.create(cc.MoveBy.create(0.1, cc.p(EloongAttributes.SPEED_HYPER_X, 0))));

			this._isAttackRunHyperStartedToCount = true;

			this._parentNode.getParent()._UI.decreaseStamina();
		}
		// Run - tired
		else if(e == cc.KEY.left && this.currentState != EloongState.RUN_TIRED_STATE && this.isOkayForPlayingNewAnimation() && this.currentMode == EloongMode.TIRED) {
			this.facingDirection = Direction.LEFT;
			this.setFlipX(false);
			this.playRunTiredAnimation();

			this.runAction(cc.RepeatForever.create(cc.MoveBy.create(0.1, cc.p(-EloongAttributes.SPEED_TIRED_X, 0))));
		}
		else if(e == cc.KEY.right && this.currentState != EloongState.RUN_TIRED_STATE && this.isOkayForPlayingNewAnimation() && this.currentMode == EloongMode.TIRED) {
			this.facingDirection = Direction.RIGHT;
			this.setFlipX(true);
			this.playRunTiredAnimation();

			this.runAction(cc.RepeatForever.create(cc.MoveBy.create(0.1, cc.p(EloongAttributes.SPEED_TIRED_X, 0))));
		}
		// Jump - normal
		if(e == cc.KEY.space && this.currentState != EloongState.JUMP_NORMAL_STATE && this.isOkayForPlayingNewAnimation() && this.currentMode == EloongMode.NORMAL && this._isDownKeyPressed) {
			this.playJumpDownNormalAnimation();
			this._isDownKeyPressed = false;
		}
		else if(e == cc.KEY.space && this.currentState != EloongState.JUMP_NORMAL_STATE && this.isOkayForPlayingNewAnimation() && this.currentMode == EloongMode.NORMAL) {
			this.playJumpNormalAnimation();
			audioEngine.playEffect(res_sfxJump);
		}

		// Jump - hyper
		if(e == cc.KEY.space && this.currentState != EloongState.JUMP_HYPER_STATE && this.isOkayForPlayingNewAnimation() && this.currentMode == EloongMode.HYPER && this._isDownKeyPressed) {
			this.playJumpDownHyperAnimation();
			this._isDownKeyPressed = false;

			this._parentNode.getParent()._UI.decreaseStamina();
		}
		else if(e == cc.KEY.space && this.currentState != EloongState.JUMP_HYPER_STATE && this.isOkayForPlayingNewAnimation() && this.currentMode == EloongMode.HYPER) {
			this.playJumpHyperAnimation();
			audioEngine.playEffect(res_sfxJump);

			this._parentNode.getParent()._UI.decreaseStamina();
		}
		// Jump - tired
		if(e == cc.KEY.space && this.currentState != EloongState.JUMP_TIRED_STATE && this.isOkayForPlayingNewAnimation() && this.currentMode == EloongMode.TIRED && this._isDownKeyPressed) {
			this.playJumpTiredAnimation();
			this._isDownKeyPressed = false;
		}
		else if(e == cc.KEY.space && this.currentState != EloongState.JUMP_TIRED_STATE && this.isOkayForPlayingNewAnimation() && this.currentMode == EloongMode.TIRED) {
			this.playJumpTiredAnimation();
			audioEngine.playEffect(res_sfxJump);
		}
		else if(e == cc.KEY.c) {
			var stamina = this._parentNode.getParent()._UI._stamina;

			if(this.currentMode == EloongMode.HYPER)
			{
				if(stamina >= EloongAttributes.THRESHOLD_TO_CHANGE_TO_HYPER)
				{
					this.currentMode = EloongMode.NORMAL;
					this.playIdleNormalAnimation();
				}
				else
				{
					this.currentMode = EloongMode.TIRED;
					this.playIdleTiredAnimation();
				}
				this._eloongHyperFire.setOpacity(0);
				audioEngine.playEffect(res_gamePowerDown);
			}
			else if((this.currentMode == EloongMode.NORMAL || this.currentMode == EloongMode.TIRED) && stamina >= EloongAttributes.THRESHOLD_TO_CHANGE_TO_HYPER)
			{
				this.currentMode = EloongMode.HYPER;
				this.playIdleHyperAnimation();
				// -- show hyperFire
				this._eloongHyperFire.setOpacity(255);
				audioEngine.playEffect(res_gamePowerUp);
			}
		}
		if(e == cc.KEY.down) {
			this._isDownKeyPressed = true;
		}

		if(e == cc.KEY.left && this.currentMode == EloongMode.HYPER)
		{
			this._parentNode.getParent()._UI.decreaseStamina();
		}
	    if(e == cc.KEY.right && this.currentMode == EloongMode.HYPER)
		{
			this._parentNode.getParent()._UI.decreaseStamina();
		}
	},
	/*
		Use this method for other non-specific animation state checking.
	*/
	isOkayForPlayingNewAnimation:function() {
		if(this.currentState == EloongState.JUMP_NORMAL_STATE ||
			this.currentState == EloongState.JUMP_HYPER_STATE ||
			this.currentState == EloongState.JUMP_TIRED_STATE ||

			this.currentState == EloongState.BEING_ATTACK_STATE ||
			this.currentState == EloongState.DIE_STATE)
		{
			return false;
		}
		else
			return true;
	},
	boundSelfWithinBoundingArea:function() {
		var level = levels[profile.level-1];
		if(this.getPositionX() - this.getContentSize().width/2 <= level.minX)
			this.setPositionX(level.minX + this.getContentSize().width/2);
		else if(this.getPositionX() + this.getContentSize().width/2 >= level.maxX)
			this.setPositionX(level.maxX - this.getContentSize().width/2);
	},
	freeFall:function() {
		if(this.currentState != EloongState.JUMP_NORMAL_STATE &&
		   this.currentState != EloongState.JUMP_HYPER_STATE &&
		   this.currentState != EloongState.JUMP_TIRED_STATE)
		{
			var currentBuilding = this._parentNode._currentBuildingEloongIsIn;
			var currentFloor = this._parentNode._currentFloorEloongIsIn;

			this.vy += 0.5;
			if(currentFloor == 1 || currentBuilding == -1)
			{
				this.vy += 0.25;

				var diff = this.getPositionY() - this.getContentSize().height/2 - LevelSettings.START_Y_POSITION_OF_ALL_BUILDINGS;
				if(diff > this.vy)
				{
					this.setPositionY(this.getPositionY() - this.vy);
				}
				else
				{
					this.setPositionY(this.getPositionY() - diff);
					this.vy = 0;
				}
			}
			else {
				var diff = this.getPositionY() - this.getContentSize().height/2 - this._parentNode._buildingsYPositions2Dim[currentBuilding-1][currentFloor-2];

				this.vy += 0.25;
				if(diff > this.vy)
				{
					this.setPositionY(this.getPositionY() - this.vy);
				}
				else
				{
					this.setPositionY(this.getPositionY() - diff);
					this.vy = 0;
				}
			}
		}
	},
	update:function(dt) {
		if(!this._parentNode.isGameOver)
		{
			if(this._isAttackRunHyperStartedToCount && this.currentMode == EloongMode.HYPER)
			{
				this._countingTimeAttackRunHyper += dt;
			}

			// charge up stamina
			if(this.currentMode == EloongMode.NORMAL ||
				this.currentMode == EloongMode.TIRED)
			{
				this._parentNode.getParent()._UI.increaseStamina();
			}

			this.updateEloongInfo();

			// update position of Eloong
			this.boundSelfWithinBoundingArea();
			this.freeFall();

			var winSize = cc.Director.getInstance().getWinSize();

		// update camera
		var level = levels[profile.level-1];
		if(this.getPositionX() - winSize.width/2 >= level.minX && this.getPositionX() + winSize.width/2 <= level.maxX)
			this._parentNode.setPositionX(-this.getPositionX() + winSize.width/2);
		if(this.getPositionY() + winSize.height/2 <= level.maxY && this.getPositionY() - winSize.height/2 >= 0)
			this._parentNode.setPositionY(-this.getPositionY() + winSize.height/2);

		// -- the fire will follow the eloong
		this._eloongHyperFire.setPositionX(this.getPositionX());
		this._eloongHyperFire.setPositionY(this.getPositionY());
		}
	},
	updateEloongInfo:function() {
		if(!this._parentNode.isGameOver)
		{
			// check the current building Eloong is in
			var isBuildingFound = false;
			for(var i=0; i<this._parentNode._buildingsArea.length; i++)
			{
				var bounding = this._parentNode._buildingsArea[i];
				if(this.getPositionX() > bounding.minX && this.getPositionX() < bounding.maxX)
				{
					isBuildingFound = true;
					this._parentNode._currentBuildingEloongIsIn = i + 1;
					break;
				}
			}
			if(!isBuildingFound)
			{
				this._parentNode._currentBuildingEloongIsIn = -1;
			}

			// check the current floor Eloong is in
			for(var b=0; b<this._parentNode._buildingsYPositions2Dim.length; b++)
			{
				var floorsArray = this._parentNode._buildingsYPositions2Dim[b];

				// check at minimum level first
				if(this.getPositionY() - this.getContentSize().height/2 == LevelSettings.START_Y_POSITION_OF_ALL_BUILDINGS)
				{
					this._parentNode._currentFloorEloongIsIn = 1;
					break;
				}
				else
				{
					for(var i=0; i<floorsArray.length; i++)
					{
						var y = floorsArray[i];

						if(this.getPositionY() - this.getContentSize().height/2 >= y &&
						   this.getPositionY() <= y + LevelSettings.FLOOR_HEIGHT)
						{
							this._parentNode._currentFloorEloongIsIn = i + 2;	// not include first floor
							break;
						}
					}
				}
			}

			// simulate physics collision
			//this.simulateGroundCollisionToSelf();
		}
	},
});

Eloong.create = function(parentNode) {
	var eloong = new Eloong();
	if(eloong && eloong.initWithIdleFrame(parentNode))
	{
		return eloong;
	}
	else
	{
		eloong = null;
		return null;
	}
}