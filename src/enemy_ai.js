function EnemyAISystem(parentNode) {
	// members
	this.parentNode = parentNode;

	this.enemyAI = new Array();

	this.eInfo = new EnemyInfo();

	//A link between enemy object & enemy AI. The object will first send an information that are relevant to AI's decision making to this system first.
	//The system then use that information to check state transition condition. 
	//Last, the system sends the current state and the object to AIWorker to have it perform things.
	this.enemyObject = new Array(); 

	this.AddEnemyIntoGame = function (enemyObj){
		var tmp = InitializeEnemyAI();
		this.enemyObject[this.enemyObject.length] = enemyObj;
		this.enemyAI[this.enemyAI.length] = tmp;
	}

	this.RemoveEnemy = function(name){
		for(var i = 0; i < this.enemyObject.length; i++){
			if(this.enemyObject[i].Name === name){
				//remove obj.
				delete this.enemyAI[i];
				delete this.enemyObject[i];

				this.enemyAI.splice(i, 1);
				this.enemyObject.splice(i, 1);
			}
		}
	};

	function GetInfoFromEachAIObject(eInfo, enemyObj, playerObj)
	{
			if(enemyObj.TargetObj != -1)
				eInfo.HasTarget = true;
			else
				{
					eInfo.HasTarget = false;
					eInfo.HostageBeingEngaged = enemyObj.TargetObj.BeingEngaged;
				}

			if(enemyObj.currentFloor === playerObj.currentFloor){
				eInfo.MainPlayerExistsOnTheSameFloor = true;
			}
			else eInfo.MainPlayerExistsOnTheSameFloor = false;

			eInfo.TargetApproachingMethod == enemyObj.MoveMethod;

			if(Math.abs(enemyObj.getPositionX() - enemyObj.TargetPosX) < 1)
				eInfo.ReachTargetXAxis = true;
			else eInfo.ReachTargetXAxis = false;

			//eInfo.Attack_Done = enemyObj.Attack_Done;
			eInfo.Attack_Done = true;
			
			//eInfo.Capture_Done = enemyObj.Capture_Done;
			eInfo.Capture_Done = true;

			eInfo.Being_Attacked = enemyObj.Being_Attacked;

			if(enemyObj.hp <= 0)
				eInfo.Zero_HP = true;

		return eInfo;
	}

	this.Update = function (playerObject, everyHostageObjects)
	{
		for(var i = 0; i < this.enemyObject.length; i++)
		{
			//1. send current state and the object to AIWorker.
			this.EnemyAIWorker(this.enemyAI[i], this.enemyObject[i], playerObject, everyHostageObjects, this.eInfo);

			//2. get info from each AI object
			playerObject.currentFloor = this.parentNode._currentFloorEloongIsIn;
			this.eInfo = GetInfoFromEachAIObject(this.eInfo, this.enemyObject[i], playerObject);

			//3. check current condition of that object based on that info & change states
			this.enemyAI[i] = this.StateTransition(this.enemyAI[i], this.eInfo);

		}

	};

	//this method puppets enemy objects.
	this.EnemyAIWorker = function(currentState, enemyObject, playerObject, everyHostageObjects, eInfo)
	{
		//console.log("pass2");
		switch(currentState.StateName){


			case "LookForPah":
				var bestCandidate = null;
				var bestCandidateDistance = null;
				eInfo.TargetType = TARGET_TYPE.None;
				enemyObject.TargetObj = -1;

				for(var i = 0; i < everyHostageObjects.length ; i++){
					  hostage = everyHostageObjects[i];
					  	global.log(hostage.currentFloor + " vs " + enemyObject.currentFloor);
					  if(hostage.currentFloor === enemyObject.currentFloor){
					  	global.log("make it eventually!");
					  	var newDistance = Math.abs(hostage.getPositionX() - enemyObject.getPositionX()) ;
					  			global.log("yoohoo! " +  bestCandidateDistance);
					  		if (bestCandidateDistance === null){
					  			bestCandidateDistance = newDistance;
					  			bestCandidate = hostage;
					  		}
					  		else if(newDistance < bestCandidateDistance){
					  			bestCandidateDistance = newDistance;
					  			bestCandidate = hostage;
					  		}
					  }
				}
				if(bestCandidate != null){
					enemyObject.TargetObj = bestCandidate;
					enemyObject.TargetPosX = bestCandidate.getPositionX();
					eInfo.TargetType = TARGET_TYPE.Pah;
				}
			break;

			case "Wandering":
				enemyObject.TargetObj = null;
					eInfo.TargetType = TARGET_TYPE.None;

				if (enemyObject.facingDirection == Direction.LEFT) 
				{
					if((enemyObject.getPositionX() - enemyObject.getContentSize().width/2) < this.parentNode._buildingsArea[0].minX ||
						eInfo.ReachTargetXAxis)
					{
						//turn your back!
						var width = this.parentNode._buildingsArea[0].maxX - this.parentNode._buildingsArea[0].minX ;
						enemyObject.TargetPosX = (Math.random() * (2 * width / 3)) + this.parentNode._buildingsArea[0].minX + (width / 3);
					}
				} 
				else //right
				{
					if((enemyObject.getPositionX() + enemyObject.getContentSize().width/2) > this.parentNode._buildingsArea[0].maxX ||
						eInfo.ReachTargetXAxis)
					{
						//turn your back!
						var width = this.parentNode._buildingsArea[0].maxX - this.parentNode._buildingsArea[0].minX ;
						enemyObject.TargetPosX = this.parentNode._buildingsArea[0].maxX - (width / 3) - (Math.random() * (2 * width / 3));
					}
				}
				enemyObject.Walk_XAxis(enemyObject.TargetPosX);
			break;

			case "WalkTowardPah":
				enemyObject.Walk_XAxis(enemyObject.TargetPosX);
			break;

			case "IsPahAvailable?":
			break;

			case "CapturePah":
				enemyObject.CaptureHostage();
			break;

			case "Disappear":
			break;

			case "StandbyForPah":
			break;

			case "LockOnMainPlayer":
				enemyObject.TargetObj = playerObject;
				eInfo.TargetType = TARGET_TYPE.Player;

				if(Math.random() < 0.50){
					//walk
					enemyObject.MoveMethod = APPROACHING_METHOD.Walking;
					if(enemyObject.getPositionX() < playerObject.getPositionX()) //target on the right
						enemyObject.TargetPosX = playerObject.getPositionX()- playerObject.getContentSize().width/2;
					else 
						enemyObject.TargetPosX = playerObject.getPositionX() + playerObject.getContentSize().width/2;
				}
				else{
					//roll
					enemyObject.MoveMethod = APPROACHING_METHOD.Rolling;
					if(enemyObject.getPositionX() < playerObject.getPositionX()) //target on the right
						enemyObject.TargetPosX = playerObject.getPositionX() + playerObject.getContentSize().width/2;
					else 
						enemyObject.TargetPosX = playerObject.getPositionX() - playerObject.getContentSize().width/2;
				}
			break;

			case "WalkToTarget":
				enemyObject.Walk_XAxis(enemyObject.TargetPosX);
			break;

			case "RollToTarget": //unfinished!!!!!
				enemyObject.Roll_XAxis(enemyObject.TargetPosX);
				//enemyObject.Walk_XAxis(enemyObject.TargetPosX);
			break;

			case "Attack":
				enemyObject.Attack();
			break;

			case "RunOutOfHP":
				RemoveEnemy(enemyObject.Name);
			return;
		}
	};


	this.StateTransition = function (currentState,eInfo)
	{
		var newState = currentState;

		global.log(currentState.StateLinks);
		for(var i = 0; i < currentState.StateLinks.length ; i++)
		{
			links = currentState.StateLinks[i];
			switch(links.LinkCondition){
				case LINK_CONDITION.LockOnPah:
					if(eInfo.HasTarget && eInfo.TargetType === TARGET_TYPE.Pah)
					{
						global.log("LockOnPah");
						global.log( newState.StateName + "  ===>  " + links.StateTarget.StateName);
						return links.StateTarget;
					}
				break;

				case LINK_CONDITION.CannotFoundPah:
					if(!eInfo.HasTarget)
					{
						global.log("CannotFoundPah");
						global.log( newState.StateName + "  ===>  " + links.StateTarget.StateName);
						return links.StateTarget;
					}
				break;

				case LINK_CONDITION.ReachPah:
					if(eInfo.ReachTargetXAxis && eInfo.TargetType === TARGET_TYPE.Pah)
					{
						global.log("ReachPah");
						global.log( newState.StateName + "  ===>  " + links.StateTarget.StateName);
						return links.StateTarget;
					}
				break;

				case LINK_CONDITION.PahAvailable:
					if(!eInfo.HostageBeingEngaged && eInfo.TargetType === TARGET_TYPE.Pah)
					{
						global.log("PahAvailable");
						global.log( newState.StateName + "  ===>  " + links.StateTarget.StateName);
						return links.StateTarget;
					}
				break;

				case LINK_CONDITION.PahNotAvailable:
					if(eInfo.HostageBeingEngaged)
					{
						global.log("PahNotAvailable");
						global.log( newState.StateName + "  ===>  " + links.StateTarget.StateName);
						return links.StateTarget;
					}
				break;

				case LINK_CONDITION.Capture_Succeed:
					if(eInfo.Capture_Done)
					{
						global.log("Capture_Succeed");
						global.log( newState.StateName + "  ===>  " + links.StateTarget.StateName);
						return links.StateTarget;
					}
				break;

				case LINK_CONDITION.FoundMainPlayer:
					if(eInfo.MainPlayerExistsOnTheSameFloor)
					{
						global.log("FoundMainPlayer");
						global.log( newState.StateName + "  ===>  " + links.StateTarget.StateName);
						return links.StateTarget;
					}
				break;

				case LINK_CONDITION.LostMainPlayer:
					if(!eInfo.MainPlayerExistsOnTheSameFloor)
					{
						global.log("LostMainPlayer");
						global.log( newState.StateName + "  ===>  " + links.StateTarget.StateName);
						return links.StateTarget;
					}
				break;

				case LINK_CONDITION.LockOnMainPlayer_ApproachByWalking:
					if(eInfo.HasTarget && eInfo.TargetApproachingMethod === APPROACHING_METHOD.Walking)
					{
						global.log("LockOnMainPlayer_ApproachByWalking");
						global.log( newState.StateName + "  ===>  " + links.StateTarget.StateName);
						return links.StateTarget;
					}
				break;

				case LINK_CONDITION.LockOnMainPlayer_ApproachByRolling:
					if(eInfo.HasTarget && eInfo.TargetApproachingMethod === APPROACHING_METHOD.Rolling)
					{
						global.log("LockOnMainPlayer_ApproachByRolling");
						global.log( newState.StateName + "  ===>  " + links.StateTarget.StateName);
						return links.StateTarget;
					}
				break;

				case LINK_CONDITION.ReachMainPlayer:
					if(eInfo.ReachTargetXAxis)
					{
						global.log("ReachMainPlayer");
						global.log( newState.StateName + "  ===>  " + links.StateTarget.StateName);
						return links.StateTarget;
					}
				break;

				case LINK_CONDITION.Attack_Succeed:
					if(eInfo.Attack_Done)
					{
						global.log("Attack_Succeed");
						global.log( newState.StateName + "  ===>  " + links.StateTarget.StateName);
						return links.StateTarget;
					}
				break;

				case LINK_CONDITION.BeingAttacked:
					if(eInfo.Being_Attacked)
					{
						global.log("BeingAttacked");
						global.log( newState.StateName + "  ===>  " + links.StateTarget.StateName);
						return links.StateTarget;
					}
				break;

				case LINK_CONDITION.RunOutOfHP:
					if(eInfo.Zero_HP)
					{
						global.log("RunOutOfHP");
						global.log( newState.StateName + "  ===>  " + links.StateTarget.StateName);
						return links.StateTarget;
					}
				break;
			}
		}

		return newState;
	};
}

 function FSMState (stateName)
 {
	this.StateName = stateName;
	this.StateLinks = new Array();

	this.AddStateLink = function(stateObj_to, condition){
		this.StateLinks[this.StateLinks.length] = new FSMLinks(stateObj_to, condition);
	};
 }

 function FSMLinks (stateObj_to, condition){
 	this.StateTarget = stateObj_to;
 	this.LinkCondition = condition;
 }


 var LINK_CONDITION = {
 	LockOnPah : 0,
 	CannotFoundPah : 1,
 	ReachPah : 2,
 	PahAvailable : 3,
 	PahNotAvailable : 4,
 	Capture_Succeed: 5,
 	FoundMainPlayer: 6,
 	LockOnMainPlayer_ApproachByRolling : 7,
 	LockOnMainPlayer_ApproachByWalking : 8,
 	ReachMainPlayer: 9,
 	Attack_Succeed : 10,
 	RunOutOfHP : 11
 };


function InitializeEnemyAI(){
	var states = new Array();
	states[0] = new FSMState("LookForPah");
	states[1] = new FSMState("Wandering");
	states[2] = new FSMState("WalkTowardPah");
	states[3] = new FSMState("IsPahAvailable?");
	states[4] = new FSMState("CapturePah");
	states[5] = new FSMState("Disappear");
	states[6] = new FSMState("StandbyForPah");
	states[7] = new FSMState("LockOnMainPlayer");
	states[8] = new FSMState("WalkToTarget");
	states[9] = new FSMState("RollToTarget");
	states[10] = new FSMState("Attack");
	states[11] = new FSMState("Dead");


	states[0].AddStateLink(states[1],LINK_CONDITION.CannotFoundPah);
	states[0].AddStateLink(states[2],LINK_CONDITION.LockOnPah);
	states[1].AddStateLink(states[7],LINK_CONDITION.FoundMainPlayer);
	states[2].AddStateLink(states[3],LINK_CONDITION.ReachPah);
	states[2].AddStateLink(states[7],LINK_CONDITION.FoundMainPlayer);
	states[3].AddStateLink(states[4],LINK_CONDITION.PahAvailable);
	states[3].AddStateLink(states[6],LINK_CONDITION.PahNotAvailable);
	states[4].AddStateLink(states[5],LINK_CONDITION.Capture_Succeed);
	states[4].AddStateLink(states[7],LINK_CONDITION.BeingAttacked);
	states[6].AddStateLink(states[3],LINK_CONDITION.PahAvailable);
	states[6].AddStateLink(states[7],LINK_CONDITION.FoundMainPlayer);
	states[7].AddStateLink(states[0],LINK_CONDITION.LostMainPlayer);
	states[7].AddStateLink(states[8],LINK_CONDITION.LockOnMainPlayer_ApproachByWalking);
	states[7].AddStateLink(states[9],LINK_CONDITION.LockOnMainPlayer_ApproachByRolling);
	states[8].AddStateLink(states[10],LINK_CONDITION.ReachMainPlayer);
	states[9].AddStateLink(states[10],LINK_CONDITION.ReachMainPlayer);
	states[10].AddStateLink(states[7],LINK_CONDITION.Attack_Succeed);

	states[0].AddStateLink(states[11],LINK_CONDITION.RunOutOfHP);
	states[1].AddStateLink(states[11],LINK_CONDITION.RunOutOfHP);
	states[2].AddStateLink(states[11],LINK_CONDITION.RunOutOfHP);
	states[3].AddStateLink(states[11],LINK_CONDITION.RunOutOfHP);
	states[4].AddStateLink(states[11],LINK_CONDITION.RunOutOfHP);
	states[5].AddStateLink(states[11],LINK_CONDITION.RunOutOfHP);
	states[6].AddStateLink(states[11],LINK_CONDITION.RunOutOfHP);
	states[7].AddStateLink(states[11],LINK_CONDITION.RunOutOfHP);
	states[8].AddStateLink(states[11],LINK_CONDITION.RunOutOfHP);
	states[9].AddStateLink(states[11],LINK_CONDITION.RunOutOfHP);
	states[10].AddStateLink(states[11],LINK_CONDITION.RunOutOfHP);

	return states[0];
}

function EnemyInfo(){
	//in every loop, these info must be updated by its owner EnemyObject.
	this.HasTarget = false;
	this.ReachTargetXAxis = false;
	this.HostageBeingEngaged = false;
	this.MainPlayerExistsOnTheSameFloor = false;
	this.TargetApproachingMethod = APPROACHING_METHOD.Walking;
	this.Attack_Done = true;
	this.Being_Attacked = false;
	this.Capture_Done = false;

	this.TargetType = TARGET_TYPE.None;
	this.Zero_HP = false;
}

var APPROACHING_METHOD = {
	Walking : 0,
	Rolling : 1
};

var TARGET_TYPE = {
	None : 0,
	Pah : 1,
	Player : 2
};