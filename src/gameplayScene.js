var GameplayLayer = cc.LayerColor.extend({
	_levelRenderer:null,
	_buildingsYPositions2Dim:[],	// array of buildings' y positions for all of their floors (2 dim)
	_buildingsArea:[],			// array of object (xMin, xMax) of each building to check for its area
	_currentBuildingEloongIsIn:1,	// default to first building
	_currentFloorEloongIsIn:1,		// default to first floor

	isGameOver:false,
	isInitialized:false,

	objectSpawner:null,
	killRecord:0,

	_eloong:null,
	_kikis:[],
	_pahs:[],

	init:function(color) {
		if(!this._super(color))
		{
			global.log("GameplayLayer's init() called failed.");
			return false;
		}

		var winSize = cc.Director.getInstance().getWinSize();

		// prepare game logic information
		this._prepareGameLogicInfomation();

		// init stuff
		this.isGameOver = false;
		this.objectSpawner = new ObjectSpawner(this);
		this.enemyAISystem = new EnemyAISystem(this);
		this.killRecord = 0;

		// render level from the current player's progress
		this._levelRenderer = new LevelRenderer(this);
		this._levelRenderer.renderLevel(profile.level);

		// add eloong into the scene
		this._eloong = Eloong.create(this);
		this._eloong.setPosition(cc.p(300,170));
		this.addChild(this._eloong, GameLayer.CHARACTER_LAYER + 1);

		// add kiki into the scene
		this._kikis = new Array();
		this._pahs = new Array();

		for(var i=0; i<3; i++)
		{
			var kiki = this.objectSpawner.spawnAndGetKiki(gmath.randomBetweenInteger(1, levels[profile.level-1].buildings[0].floorVertical), this.enemyAISystem);
			this.addChild(kiki, GameLayer.CHARACTER_LAYER);
			this._kikis.push(kiki);
		}
		for (var i=0; i<2; i++)
		{
			var pah = this.objectSpawner.spawnAndGetPah(
				gmath.randomBetweenInteger(1, levels[profile.level-1].buildings[0].floorVertical),
				gmath.randomBetweenInteger(1,2));
			this.addChild(pah, GameLayer.CHARACTER_LAYER);

			this._pahs.push(pah);
		}

		this.setKeyboardEnabled(true);
		this.isInitialized = true;

		//this.scheduleUpdate();
		this.schedule(this.updateInterval, 1/3);
		this.schedule(this.spawnNewEnemies, 5);
		this.scheduleUpdate();

		return true;
	},

	updateInterval:function(){
		if(!this.isGameOver)
			this.enemyAISystem.Update(this._eloong, this._pahs);
	},

	spawnNewEnemies:function() {
		// count live kiki
		var count = 0;
		for(var i=0; i<this._kikis.length; i++)
		{
			if(this._kikis[i].hp > 0)
				count++;
		}

		if(count < 6)
		{
			for(var i=0; i<3; i++)
			{
				var kiki = this.objectSpawner.spawnAndGetKiki(
					gmath.randomBetweenInteger(1, levels[profile.level-1].buildings[0].floorVertical), this.enemyAISystem);
				this.addChild(kiki, GameLayer.CHARACTER_LAYER);
				this._kikis.push(kiki);
			}
		}
	},

	update:function(dt) {
		if(this.isGameOver)
		{
			// add cleared layer
			this.getParent().addChild(ClearedLayer.create(this.killRecord + ""), 10);
		}
	},

	/*
		Prepare game logic information.
	*/
	_prepareGameLogicInfomation:function() {
		this._buildingsYPositions2Dim = new Array();
		this._buildingsArea = new Array();

		var rightEdgeOfPreviousBuilding = 0;

		for(var i=0; i<levels[profile.level-1].buildings.length; i++)
		{
			var level = levels[profile.level-1];

			// #1 Prepare buildings' y-pos (not include ground level)
			var yPosArray = new Array();
			for(var y=1; y<level.buildings[i].floorVertical; y++)
			{
				yPosArray.push(LevelSettings.START_Y_POSITION_OF_ALL_BUILDINGS + y*LevelSettings.FLOOR_HEIGHT);
			}
			this._buildingsYPositions2Dim.push(yPosArray);

			// #2 Prepare buildings' area (only in x-direction)
			var minX = LevelSettings.START_X_POSITION_OF_FIRST_BUILDING - 35 + i*rightEdgeOfPreviousBuilding + i*LevelSettings.SPACING_BETWEEN_BUILDING;
			var maxX = LevelSettings.START_X_POSITION_OF_FIRST_BUILDING + 35 + (level.buildings[i].floorHorizontal-1)*LevelSettings.FLOOR_WIDTH_UNIT_PIXEL + LevelSettings.FLOOR_WIDTH_UNIT_PIXEL + i*rightEdgeOfPreviousBuilding + i*LevelSettings.SPACING_BETWEEN_BUILDING;

			this._buildingsArea.push(new BuildingArea(minX, maxX));

			rightEdgeOfPreviousBuilding = maxX;
		}
	},

	// -- keyboard ---
	onKeyUp:function (e) {
		if(!this.isGameOver) {
			this._eloong.onKeyUp(e);
		}
	},
	onKeyDown:function (e) {
		if(!this.isGameOver)
		{
			// update to eloong's key control
			this._eloong.onKeyDown(e);
		}
	}
});

var GameplayScene = cc.Scene.extend({
	_UI:null,

	onEnter:function() {
		this._super();

		var backgroundLayer = cc.LayerColor.create(cc.c4b(0x73,0xD1,0xF5,0xFF));

		var layer1 = new GameplayLayer();
        layer1.init(cc.c4b(0,0,0,0)); //0x73,0xD1,0xF5,0xFF

        this.addChild(backgroundLayer);
        this.addChild(layer1);

        // add UI
		this._UI = new UILayer();
		// this._UI.init(cc.c4b(0,0,0,0));
		this._UI.initWithParentNode(layer1);
		this.addChild(this._UI);
	}
});