// Global settings and Functions
var global = {
	// Settings
	debug:0,
	music:1,
	sfx:1,
	showHint:1,

	// Functions (mostly wrapper and utillities)
	log:function (msg) {
		if(global.debug == 1)
			cc.log(msg);
	},
	playBackgroundMusic:function (path, isLoop) {
		if(global.music == 1)
			cc.AudioEngine.getInstance().playBackgroundMusic(path, isLoop);
	},
	playEffect:function (path) {
		if(global.sfx == 1)
		{
			var soundId = cc.AudioEngine.getInstance().playEffect(path);
			return soundId;
		}
	},
	stopBackgroundMusic:function (releaseData) {
		if(global.music == 1)
			cc.AudioEngine.getInstance().stopBackgroundMusic(releaseData);
	},
	stopEffect:function (path) {
		if(global.music == 1)
			cc.AudioEngine.getInstance().stopEffect(path);
	},
	rewindBackgroundMusic:function () {
		if(global.music == 1)
			cc.AudioEngine.getInstance().rewindBackgroundMusic();
	},
	isBackgroundMusicPlaying:function () {
		if(global.music == 1)
			return cc.AudioEngine.getInstance().isBackgroundMusicPlaying();
	},
	// Spritesheet & Frame related utilities function
	loadSpriteFrames:function (plist) {
		cc.SpriteFrameCache.getInstance().addSpriteFrames(plist);
	},
	unloadSpriteFrames:function (plist) {
		cc.SpriteFrameCache.getInstance().removeSpriteFramesFromFile(plist);
	},
	getSpriteFrame:function (name) {
		return cc.SpriteFrameCache.getInstance().getSpriteFrame(name);
	},
	adjustSizeForWindow:function () {
        var margin = document.documentElement.clientWidth - document.body.clientWidth;
        if (document.documentElement.clientWidth < cc.originalCanvasSize.width) {
            cc.canvas.width = cc.originalCanvasSize.width;
        } else {
            cc.canvas.width = document.documentElement.clientWidth - margin;
        }
        if (document.documentElement.clientHeight < cc.originalCanvasSize.height) {
            cc.canvas.height = cc.originalCanvasSize.height;
        } else {
            cc.canvas.height = document.documentElement.clientHeight - margin;
        }

        var xScale = cc.canvas.width / cc.originalCanvasSize.width;
        var yScale = cc.canvas.height / cc.originalCanvasSize.height;
        if (xScale > yScale) {
            xScale = yScale;
        }
        cc.canvas.width = cc.originalCanvasSize.width * xScale;
        cc.canvas.height = cc.originalCanvasSize.height * xScale;
        var parentDiv = document.getElementById("Cocos2dGameContainer");
        if (parentDiv) {
            parentDiv.style.width = cc.canvas.width + "px";
            parentDiv.style.height = cc.canvas.height + "px";
        }
        cc.renderContext.translate(0, cc.canvas.height);
        cc.renderContext.scale(xScale, xScale);
        cc.Director.getInstance().setContentScaleFactor(xScale);
    }
};

// Math utilities
var gmath = {
	// return 1.0 for left | -1.0 for right
	randomDirectionValue:function() {
		var dir = 1.0;
		if(Math.random() > 0.5)
			dir = -1.0;

		return dir;
	},
	// random [min, max]
	randomBetween:function(min, max) {
		return min + Math.random() * (max-min);
	},
	randomBetweenInteger:function(min, max) {
		return Math.floor((Math.random()*max)+min);
	},
	// Input is cc.Point
	getLengthFrom:function(vec1, vec2) {
		return Math.sqrt( Math.pow(vec1.x - vec2.x, 2), Math.pow(vec1.y - vec2.y, 2));
	},
	// Lerp
	lerp:function(a, b, u) {
    	return (1 - u) * a + u * b;
    }
};