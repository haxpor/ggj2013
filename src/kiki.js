var KikiState = {
	IDLE_STATE: 1,
	WALK_STATE: 2,
	WHEEL_STATE: 3,
	BEING_ATTACK_STATE: 4,
	CATCH_STATE: 5,
	DIE_STATE: 6,
	ATTACK_STATE: 7
}

var KikiSettings = {
	SPEED: 3
}

var Kiki = cc.Sprite.extend({
	_parentNode:null,

	_idleAnim:null,
	_walkAnim:null,
	_wheelAnim:null,
	_beingAttack:null,
	_catchAnim:null,
	_dieAnim:null,
	_attackAnim:null,

	_targetX: -1,

	currentState: null,
	facingDirection: Direction.LEFT,
	currentFloor:-1,
	hp:10,

	TargetObj:-1,
	TargetPosX:-1,
	TargetPosY:-1,
	Teleport_Done: false,
	Attack_Done: false,
	Capture_Done: false,
	Name: null,
	MoveMethod: null,

	initWithIdleFrame:function (parentNode) {
		this._parentNode = parentNode;

		// load kiki spritesheet
		global.loadSpriteFrames(res_miscSpritesheetPlist);

		if(!this.initWithSpriteFrame(global.getSpriteFrame("kiki_idle_1.png")))
		{
			global.log("Kiki's initWithSpace() error.");
			// unload eloong spritesheet
			global.unloadSpriteFrames(res_miscSpritesheetPlist);

			return false;
		}

		// init stuff
		this._targetX = -1;
		this.currentFloor = -1;
		this.facingDirection = Direction.LEFT;
		this.TargetObj = -1;
		this.TargetPosX = -1;
		this.TargetPosY = -1;
		this.Teleport_Done = false;
		this.Attack_Done = false;
		this.Capture_Done = false;
		this.Name = "";
		this.MoveMethod = APPROACHING_METHOD.Walking;

		// create animations
		var frames = new Array();
		var animate = null;

		// -- idle
		frames.push(global.getSpriteFrame("kiki_idle_1.png"));
		frames.push(global.getSpriteFrame("kiki_idle_2.png"));
		animate = cc.Animate.create(cc.Animation.create(frames, 1/7.0));
		this._idleAnim = cc.RepeatForever.create(animate);

		// -- walk
		frames.length = 0;
		frames.push(global.getSpriteFrame("kiki_walk_1.png"));
		frames.push(global.getSpriteFrame("kiki_walk_2.png"));
		frames.push(global.getSpriteFrame("kiki_walk_3.png"));
		frames.push(global.getSpriteFrame("kiki_walk_4.png"));
		animate = cc.Animate.create(cc.Animation.create(frames, 1/7.0));
		this._walkAnim = cc.RepeatForever.create(animate);

		// -- wheel
		frames.length = 0;
		frames.push(global.getSpriteFrame("kiki_wheel_1.png"));
		frames.push(global.getSpriteFrame("kiki_wheel_2.png"));
		frames.push(global.getSpriteFrame("kiki_wheel_3.png"));
		frames.push(global.getSpriteFrame("kiki_wheel_4.png"));
		animate = cc.Animate.create(cc.Animation.create(frames, 1/7.0));
		this._wheelAnim = cc.RepeatForever.create(animate);

		// -- being attack
		frames.length = 0;
		frames.push(global.getSpriteFrame("kiki_being_attacked.png"));
		animate = cc.Animate.create(cc.Animation.create(frames, 1/7.0));
		this._beingAttack = cc.Repeat.create(animate, 1);

		// -- catch
		frames.length = 0;
		frames.push(global.getSpriteFrame("kiki_catch.png"));
		animate = cc.Animate.create(cc.Animation.create(frames, 1/7.0));
		this._catchAnim = cc.Sequence.create(
			cc.Repeat.create(animate, 1),
			cc.DelayTime.create(1.0),
			cc.CallFunc.create(this.warpOut, this)
		);

		// -- die
		frames.length = 0;
		frames.push(global.getSpriteFrame("kiki_die.png"));
		animate = cc.Animate.create(cc.Animation.create(frames, 1/7.0));
		this._dieAnim = cc.Sequence.create(
			cc.Repeat.create(animate, 1),
			cc.CallFunc.create(this._blinkAndDisappear, this)
			);

		// -- attack
		frames.length = 0;
		frames.push(global.getSpriteFrame("kiki_attack_1.png"));
		frames.push(global.getSpriteFrame("kiki_attack_2.png"));
		animate = cc.Animate.create(cc.Animation.create(frames, 1/7.0));
		this._attackAnim = cc.Sequence.create(
			cc.Repeat.create(animate, 1),
			cc.CallFunc.create(this.playIdleAnimation, this)
			);

		// unload kiki spritesheet
		global.unloadSpriteFrames(res_miscSpritesheetPlist);

		this.playIdleAnimation();

		return true;
	},

	playIdleAnimation:function() {
		if(this.currentState != KikiState.IDLE_STATE)
		{
			this.stopAllActions();

			this.currentState = KikiState.IDLE_STATE;
			this.runAction(this._idleAnim);
		}
	},
	playIdleAnimationNoStopActions:function() {
		if(this.currentState != KikiState.IDLE_STATE)
		{
			this.currentState = KikiState.IDLE_STATE;
			this.runAction(this._idleAnim);
		}
	},
	playWalkAnimation:function() {
		if(this.currentState != KikiState.WALK_STATE)
		{
			this.stopAllActions();

			this.currentState = KikiState.WALK_STATE;
			this.runAction(this._walkAnim);
		}
	},
	playWheelAnimation:function() {
		if(this.currentState != KikiState.WHEEL_STATE)
		{
			this.stopAllActions();

			this.currentState = KikiState.WHEEL_STATE;
			this.runAction(this._wheelAnim);
		}
	},
	playWheelAnimationNoStopActions:function() {
		if(this.currentState != KikiState.WHEEL_STATE)
		{
			this.currentState = KikiState.WHEEL_STATE;
			this.runAction(this._wheelAnim);
		}
	},
	playBeingAttackAnimation:function() {
		if(this.currentState != KikiState.BEING_ATTACK_STATE)
		{
			this.stopAllActions();

			this.currentState = KikiState.BEING_ATTACK_STATE;
			this.runAction(
				cc.Sequence.create(
					this._beingAttack,
					cc.DelayTime.create(0.2),
					cc.CallFunc.create(this.playIdleAnimation, this))
				);
		}
	},
	playCatchAnimation:function() {
		if(this.currentState != KikiState.CATCH_STATE)
		{
			this.stopAllActions();

			this.currentState = KikiState.CATCH_STATE;
			this.runAction(this._catchAnim);
		}
	},
	playDieAnimation:function() {
		if(this.currentState != KikiState.DIE_STATE)
		{
			this.stopAllActions();

			this.currentState = KikiState.DIE_STATE;
			this.runAction(this._dieAnim);
			audioEngine.playEffect(res_sfxKikiDie);
		}
	},
	playAttackAnimation:function() {
		if(this.currentState != KikiState.ATTACK_STATE)
		{
			this.stopAllActions();

			this.currentState = KikiState.ATTACK_STATE;
			this.runAction(this._attackAnim);
		}
	},
	_blinkAndDisappear:function() {
		var blink = cc.Blink.create(1.5, 10);
		this.runAction(cc.Sequence.create(blink, cc.CallFunc.create(this._removeFromParentAndCleanup, this)));
	},
	_removeFromParentAndCleanup:function() {
		this.removeFromParent(true);
	},
	warpOut:function() {
		// TODO: Add something to warp out ...
		global.log("something");
	},
	_checkIsReachXDestination:function() {
		if(Math.abs(this.getPositionX() - this._targetX) < KikiSettings.SPEED)
			this.playIdleAnimation();
		else
			this.Walk_XAxis(this._targetX);
	},
	// API
	Walk_XAxis:function(x) {
		this._targetX = x;
		var dir = 1.0;
		if(this._targetX > this.getPositionX())
		{
			this.setFlipX(true);
			this.facingDirection = Direction.RIGHT;
		}
		else if(this._targetX < this.getPositionX())
		{
			this.setFlipX(false);
			dir = -1.0;
			this.facingDirection = Direction.LEFT;
		}

		var diff = Math.abs(this.getPositionX() - this._targetX);
		if(diff > KikiSettings.SPEED)
		{
			diff = KikiSettings.SPEED;
		}

		this.runAction(
			cc.Sequence.create(
				cc.Spawn.create(
					cc.MoveBy.create(0.05, cc.p(diff * dir, 0)),
					cc.CallFunc.create(this.playWalkAnimationButNotStopActions, this)
				),
				cc.CallFunc.create(this._checkIsReachXDestination, this))
			);
	},
	CaptureHostage:function(){
		this.playCatchAnimation();
	},
	Attack:function() {
		if(this.currentState != KikiState.ATTACK_STATE)
		{
			this.playAttackAnimation();
		
			// check if hit with eloong
			var eloong = this._parentNode._eloong;
			if(eloong.currentState != EloongState.JUMP_NORMAL_STATE &&
			   eloong.currentState != EloongState.JUMP_HYPER_STATE &&
			   eloong.currentState != EloongState.JUMP_TIRED_STATE)
			{
				var kikiRect = cc.RectMake(
					this.getPositionX()-this.getContentSize().width/2,
					this.getPositionY()-this.getContentSize().height/2,
					this.getContentSize().width,
					this.getContentSize().height);
				var eloongRect = cc.RectMake(
					eloong.getPositionX()-this.getContentSize().width/2,
					eloong.getPositionY()-this.getContentSize().height/2,
					eloong.getContentSize().width,
					eloong.getContentSize().height
					);

				if(cc.rectIntersectsRect(kikiRect, eloongRect))
				{
					// hit to Eloong!
					if(eloong.currentMode == EloongMode.HYPER)
						this._parentNode.getParent()._UI.getLessDamage();
					else
						this._parentNode.getParent()._UI.getDamage();
					
					this._parentNode.getParent()._UI.wasHitByKiki();

					// need to return to normal state or not ?
					if(this._parentNode.getParent()._UI._hp > 0)
						this._parentNode._eloong.playBeingAttackAnimation();
				}
			}
		}
	},
	// return true if die, else not yet.
	BeingHit:function(reducePoint) {
		this.hp -= reducePoint;

		if(this.hp <= 0)
		{
			this.runAction(cc.Sequence.create(
				cc.CallFunc.create(this.playBeingAttackAnimation, this),
				cc.CallFunc.create(this.playDieAnimation, this)));

			return true;
		}
		else
		{
			this.playBeingAttackAnimation();

			return false;
		}
	},
	Roll_XAxis:function(x) {
		this._targetX = x;
		var dir = 1.0;
		if(this._targetX > this.getPositionX())
		{
			this.setFlipX(true);
			this.facingDirection = Direction.RIGHT;
		}
		else if(this._targetX < this.getPositionX())
		{
			this.setFlipX(false);
			dir = -1.0;
			this.facingDirection = Direction.LEFT;
		}

		var diff = Math.abs(this.getPositionX() - this._targetX);
		if(diff > KikiSettings.SPEED)
		{
			diff = KikiSettings.SPEED;
		}

		this.runAction(
			cc.Sequence.create(
				cc.Spawn.create(
					cc.MoveBy.create(0.04, cc.p(diff * dir, 0)),
					cc.CallFunc.create(this.playWheelAnimation, this)
				),
				cc.CallFunc.create(this._checkIsReachXDestination, this))
			);
	},
});

Kiki.create = function(parentNode) {
	var kiki = new Kiki();
	if(kiki && kiki.initWithIdleFrame(parentNode))
	{
		return kiki;
	}
	else
	{
		kiki = null;
		return null;
	}
}