/*
	LevelRenderer
	It reads levels.js file and render out the whole level.
	It also uses LevelSettings as a configuration to draw level.
*/
var GameLayer = {
	WALL_LAYER:1,
	INTERIOR_LAYER:5,
	FLOOR_LAYER:10,
	FLOORRIM_LAYER:15,
	STAIRS_LAYER:20,
	CHARACTER_LAYER:25,
	HUD_LAYER:30,
	OVERLAY_LAYER:35
}

function LevelRenderer (parentNode) {
	// members
	this.parentNode = parentNode;
	this._rightEdgeOfPreviousBuilding = 0;
}

LevelRenderer.prototype.renderLevel = function(levelNumber) {
	// load levels spritesheet
	global.loadSpriteFrames(res_levelsSpritesheetPlist);

	// get level information
	var level = levels[levelNumber-1];

	for(var b=0; b<level.buildings.length; b++)
	{
		var building = level.buildings[b];

		for(var i=0; i<building.floorHorizontal; i++)
		{
			for(var j=0; j<building.floorVertical; j++)
			{
				// #1 Draw Wall
				var wall = this.createWallSpriteOfStyle(building.wallStyles[i][j].style);
				wall.setPosition(
					LevelSettings.START_X_POSITION_OF_FIRST_BUILDING + i*LevelSettings.FLOOR_WIDTH_UNIT_PIXEL + b*this._rightEdgeOfPreviousBuilding + b*LevelSettings.SPACING_BETWEEN_BUILDING,
					LevelSettings.START_Y_POSITION_OF_ALL_BUILDINGS + j*LevelSettings.FLOOR_HEIGHT);
				this.parentNode.addChild(wall, GameLayer.WALL_LAYER);

				// - update right edge of previous building
				if(i == building.floorHorizontal - 1 && j == building.floorVertical - 1)
					this._rightEdgeOfPreviousBuilding = wall.getPositionX() + LevelSettings.FLOOR_WIDTH_UNIT_PIXEL;

				// #2 Draw interior
				if(building.interiorStyles[i][j].style != 0 || building.interiorStyles[i][j].style != "0") {
					var interior = this.createInteriorSpriteOfStyle(building.interiorStyles[i][j].style);
					interior.setPosition(wall.getPosition());
					this.parentNode.addChild(interior, GameLayer.INTERIOR_LAYER);
				}

				// #3 Draw floor
				var floor = this.createFloorSpriteOfStyle(building.floorStyles[i][j].style);
				floor.setPosition(wall.getPosition());
				this.parentNode.addChild(floor, GameLayer.FLOOR_LAYER);

				// #4 Draw floor-rim
				if(j != 0)
				{
					if(i == 0)
					{
						var floorRim = this.createFloorRimSpriteOfStyle(building.floorRimStyles[0][0].style, true);
						floorRim.setPosition(cc.p(wall.getPositionX() - LevelSettings.FLOOR_WIDTH_UNIT_PIXEL, wall.getPositionY()));
						this.parentNode.addChild(floorRim, GameLayer.FLOORRIM_LAYER);
					} 
					else if(i == building.floorHorizontal - 1) {
						var floorRim = this.createFloorRimSpriteOfStyle(building.floorRimStyles[1][j].style, false);
						floorRim.setPosition(cc.p(wall.getPositionX() + LevelSettings.FLOOR_WIDTH_UNIT_PIXEL, wall.getPositionY()));
						this.parentNode.addChild(floorRim, GameLayer.FLOORRIM_LAYER);
					}
				}
			}
		}

		// #5 Draw stairs
		for(var i=0; i<building.stairs.length; i++)
		{
			var stairsInfo = building.stairs[i];

			var stairs = this.createStairsSpriteOfStyle(building.stairsStyle, stairsInfo.isFlip);
			if(b == 0)
			{
				stairs.setPosition(
					cc.p(LevelSettings.START_X_POSITION_OF_FIRST_BUILDING + (stairsInfo.x-1)*LevelSettings.FLOOR_WIDTH_UNIT_PIXEL + b*LevelSettings.SPACING_BETWEEN_BUILDING,
						 LevelSettings.START_Y_POSITION_OF_ALL_BUILDINGS + (stairsInfo.y-1)*LevelSettings.FLOOR_HEIGHT));
			}
			else
			{
				stairs.setPosition(
					cc.p(LevelSettings.START_X_POSITION_OF_FIRST_BUILDING + (stairsInfo.x-1)*LevelSettings.FLOOR_WIDTH_UNIT_PIXEL + (level.buildings[b-1].floorHorizontal + 1)*LevelSettings.FLOOR_WIDTH_UNIT_PIXEL + b*LevelSettings.SPACING_BETWEEN_BUILDING,
						 LevelSettings.START_Y_POSITION_OF_ALL_BUILDINGS + (stairsInfo.y-1)*LevelSettings.FLOOR_HEIGHT));
			}
			this.parentNode.addChild(stairs, GameLayer.STAIRS_LAYER);
		}
	}

	// unload level spritesheet
	global.unloadSpriteFrames(res_levelsSpritesheetPlist);

	// - Add ground sprites
	global.loadSpriteFrames(res_miscSpritesheetPlist);

	// find number of screens to draw ground sprites
	var numScreen = Math.ceil(level.maxX / 800);

	for(var i=0; i<=7*numScreen; i++)
	{
		var ground = cc.Sprite.createWithSpriteFrameName("soil.png");
		ground.setAnchorPoint(cc.p(0.0, 0.0));
		ground.setPosition(cc.p(i*LevelSettings.FLOOR_WIDTH_UNIT_PIXEL + level.minX, 0));
		this.parentNode.addChild(ground, GameLayer.HUD_LAYER);
	}

	global.unloadSpriteFrames(res_miscSpritesheetPlist);
}

// All of the following functions need to have spritesheet (contains frames) loaded before calling.
LevelRenderer.prototype.createWallSpriteOfStyle = function(style) {
	var sp = cc.Sprite.createWithSpriteFrameName("wall_" + style + ".png");
	sp.setAnchorPoint(cc.p(0.0, 0.0));
	return sp;
}

LevelRenderer.prototype.createInteriorSpriteOfStyle = function(style) {
	if(style != 0 || style != "0") {
		var sp = cc.Sprite.createWithSpriteFrameName("interior_" + style + ".png");
		sp.setAnchorPoint(cc.p(0.0, 0.0));
		return sp;
	}
}

LevelRenderer.prototype.createFloorSpriteOfStyle = function(style) {
	var sp = cc.Sprite.createWithSpriteFrameName("floor_" + style + ".png");
	sp.setAnchorPoint(cc.p(0.0, 0.0));
	return sp;
}

LevelRenderer.prototype.createFloorRimSpriteOfStyle = function(style, isFlip) {
	var sp = cc.Sprite.createWithSpriteFrameName("floor-rim_" + style + ".png");
	sp.setAnchorPoint(cc.p(0.0, 0.0));
	sp.setFlipX(isFlip);
	return sp;
}

LevelRenderer.prototype.createStairsSpriteOfStyle = function(style, isFlip) {
	var sp = cc.Sprite.createWithSpriteFrameName("stairs_" + style + ".png");
	sp.setAnchorPoint(cc.p(0.0, 0.0));
	sp.setFlipX(isFlip);
	return sp;
}
// End of Section