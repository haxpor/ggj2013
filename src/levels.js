// ## Level Design ## //
/*
	Level settings note
	- Width of floor per 1 unit is the same as width of stair
	- Height between floor is the same as height of stair
	- One bridge chunk equals SPACING_BETWEEN_BUILDING (in pixel)
	- FLOOR_WIDTH_UNIT_PIXEL is fixed value
	- FLOOR_HEIGHT is fixed value
	- Ground level is START_Y_POSITION_OF_ALL_BUILDINGS
*/
var LevelSettings = {
	FLOOR_WIDTH_UNIT_PIXEL: 120,	// width of floor per 1 unit, same as width of stair. It's fixed value.
	FLOOR_HEIGHT: 120,	// it's fixed value
	MININUM_NUMBER_OF_FLOOR_WIDTH_CHUNKS: 3,
	MININUM_NUMBER_OF_FLOORS: 3,
	SPACING_BETWEEN_BUILDING: 200,	// in pixels
	START_X_POSITION_OF_FIRST_BUILDING: 120,
	START_Y_POSITION_OF_ALL_BUILDINGS: 120
}

/*
	Level data format note
	bridgePositions -> the position is expressed in non-index. So it starts at {1,1} at the left-most on first floor.
*/
var levels = [
// Level 1
{
	buildings: [
		{
			floorHorizontal: LevelSettings.MININUM_NUMBER_OF_FLOOR_WIDTH_CHUNKS + 2,
			floorVertical: LevelSettings.MININUM_NUMBER_OF_FLOORS + 2,	// not include roof-floor
			stairs: [],
			wallStyles: [
				[{style: "2"}, {style: "2"}, {style: "1"}, {style: "3"}, {style: "4"}],
				[{style: "2"}, {style: "2"}, {style: "1"}, {style: "3"}, {style: "4"}],
				[{style: "2"}, {style: "2"}, {style: "1"}, {style: "3"}, {style: "4"}],
				[{style: "2"}, {style: "2"}, {style: "1"}, {style: "3"}, {style: "4"}],
				[{style: "2"}, {style: "2"}, {style: "1"}, {style: "3"}, {style: "4"}]
			],
			interiorStyles: [
				[{style: "1"}, {style: "2"}, {style: "5"}, {style: "0"}, {style: "6"}],
				[{style: "5"}, {style: "0"}, {style: "5"}, {style: "7"}, {style: "6"}],
				[{style: "3"}, {style: "7"}, {style: "5"}, {style: "5"}, {style: "6"}],
				[{style: "8"}, {style: "0"}, {style: "5"}, {style: "3"}, {style: "6"}],
				[{style: "0"}, {style: "3"}, {style: "5"}, {style: "4"}, {style: "6"}]
			],
			floorStyles: [
				[{style: "1"}, {style: "1"}, {style: "1"}, {style: "1"}, {style: "1"}],
				[{style: "1"}, {style: "1"}, {style: "1"}, {style: "1"}, {style: "1"}],
				[{style: "1"}, {style: "1"}, {style: "1"}, {style: "1"}, {style: "1"}],
				[{style: "1"}, {style: "1"}, {style: "1"}, {style: "1"}, {style: "1"}],
				[{style: "1"}, {style: "1"}, {style: "1"}, {style: "1"}, {style: "1"}]
			],
			floorRimStyles: [
				[{style: "1"}, {style: "1"}, {style: "1"}, {style: "1"}, {style: "1"}],	// left-side of building
				[{style: "1"}, {style: "1"}, {style: "1"}, {style: "1"}, {style: "1"}]	// right-side of buildilng
			],
			stairsStyle: "1"
		}
	],
	bridgeFloors: [
		{floor: 2, buildingLeft: 1, buildingRight: 2}
	],
	minX: -200,
	maxX: 1300,
	maxY: 600 * 5
},
// Level 2
{
	buildings: [
		{
			floorHorizontal: LevelSettings.MININUM_NUMBER_OF_FLOOR_WIDTH_CHUNKS + 2,
			floorVertical: LevelSettings.MININUM_NUMBER_OF_FLOORS + 2,	// not include roof-floor
			stairs: [	// non-indice
				{x: 2, y: 1, isFlip: false},
				{x: 3, y: 2, isFlip: true}
			],
			wallStyles: [
				[{style: "test"}, {style: "test"}, {style: "test"}, {style: "test"}, {style: "test"}],
				[{style: "test"}, {style: "test"}, {style: "test"}, {style: "test"}, {style: "test"}],
				[{style: "test"}, {style: "test"}, {style: "test"}, {style: "test"}, {style: "test"}],
				[{style: "test"}, {style: "test"}, {style: "test"}, {style: "test"}, {style: "test"}],
				[{style: "test"}, {style: "test"}, {style: "test"}, {style: "test"}, {style: "test"}]
			],
			interiorStyles: [
				[{style: "test"}, {style: "test"}, {style: "test"}, {style: "test"}, {style: "test"}],
				[{style: "test"}, {style: "test"}, {style: "test"}, {style: "test"}, {style: "test"}],
				[{style: "test"}, {style: "test"}, {style: "test"}, {style: "test"}, {style: "test"}],
				[{style: "test"}, {style: "test"}, {style: "test"}, {style: "test"}, {style: "test"}],
				[{style: "test"}, {style: "test"}, {style: "test"}, {style: "test"}, {style: "test"}]
			],
			floorStyles: [
				[{style: "test"}, {style: "test"}, {style: "test"}, {style: "test"}, {style: "test"}],
				[{style: "test"}, {style: "test"}, {style: "test"}, {style: "test"}, {style: "test"}],
				[{style: "test"}, {style: "test"}, {style: "test"}, {style: "test"}, {style: "test"}],
				[{style: "test"}, {style: "test"}, {style: "test"}, {style: "test"}, {style: "test"}],
				[{style: "test"}, {style: "test"}, {style: "test"}, {style: "test"}, {style: "test"}]
			],
			floorRimStyles: [
				[{style: "test"}, {style: "test"}, {style: "test"}, {style: "test"}, {style: "test"}],
				[{style: "test"}, {style: "test"}, {style: "test"}, {style: "test"}, {style: "test"}]
			],
			stairsStyle: "test"
		},
		{
			floorHorizontal: LevelSettings.MININUM_NUMBER_OF_FLOOR_WIDTH_CHUNKS,
			floorVertical: LevelSettings.MININUM_NUMBER_OF_FLOORS,	// not include roof-floor
			stairs: [
				{x: 1, y: 1, isFlip: false},
				{x: 2, y: 2, isFlip: false}
			],
			wallStyles: [
				[{style: "test"}, {style: "test"}, {style: "test"}],
				[{style: "test"}, {style: "test"}, {style: "test"}],
				[{style: "test"}, {style: "test"}, {style: "test"}]
			],
			interiorStyles: [
				[{style: "test"}, {style: "test"}, {style: "test"}],
				[{style: "test"}, {style: "test"}, {style: "test"}],
				[{style: "test"}, {style: "test"}, {style: "test"}]
			],
			floorStyles: [
				[{style: "test"}, {style: "test"}, {style: "test"}],
				[{style: "test"}, {style: "test"}, {style: "test"}],
				[{style: "test"}, {style: "test"}, {style: "test"}]
			],
			floorRimStyles: [
				[{style: "test"}, {style: "test"}, {style: "test"}],
				[{style: "test"}, {style: "test"}, {style: "test"}]
			],
			stairsStyle: "test"
		}
	],
	bridgeFloors: [
		{floor: 2, buildingLeft: 1, buildingRight: 2}
	],
	minX: -200,
	maxX: 800 * 5,
	maxY: 800 * 5
}
];