function ObjectSpawner (parentNode) {
	// members
	this.parentNode = parentNode;
	this.kikiNumberCount = 0;
}

ObjectSpawner.prototype.spawnAndGetKiki = function(atFloor, enemyAISystem) {
	var kiki = Kiki.create(this.parentNode);
	kiki.currentFloor = atFloor;
	kiki.Name = this.kikiNumberCount + "";
	this.kikiNumberCount++;

	enemyAISystem.AddEnemyIntoGame(kiki);

	// fix at the first building
	if(atFloor == 1)
		kiki.setPositionY(LevelSettings.START_Y_POSITION_OF_ALL_BUILDINGS + kiki.getContentSize().height/2);
	else
		kiki.setPositionY(this.parentNode._buildingsYPositions2Dim[0][atFloor - 2] + kiki.getContentSize().height/2);

	// random in x-position
	kiki.setPositionX(
		 gmath.randomBetween(
		 this.parentNode._buildingsArea[0].minX + 30 + kiki.getContentSize().width/2,
		 this.parentNode._buildingsArea[0].maxX + 30 - kiki.getContentSize().width/2)
	);

	return kiki;
}

ObjectSpawner.prototype.spawnAndGetPah = function(atFloor, style) {
	var pah = Pah.create(this.parentNode, style);
	pah.currentFloor = atFloor;

	// fix at the first building
	if(atFloor == 1)
		pah.setPositionY(LevelSettings.START_Y_POSITION_OF_ALL_BUILDINGS + pah.getContentSize().height/2);
	else
		pah.setPositionY(this.parentNode._buildingsYPositions2Dim[0][atFloor - 2] + pah.getContentSize().height/2);

	// random in x-position
	pah.setPositionX(
		gmath.randomBetween(
			this.parentNode._buildingsArea[0].minX + 30 + pah.getContentSize().width/2,
		 	this.parentNode._buildingsArea[0].maxX + 30 - pah.getContentSize().width/2)
		);

	// randomn x-flipping
	if(gmath.randomDirectionValue() > 0.0)
		pah.setFacingDirection(Direction.LEFT);
	else
		pah.setFacingDirection(Direction.RIGHT);

	return pah;
}