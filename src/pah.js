var PahState = {
	IDLE_STATE: 1,
	WALK_STATE: 2
}

var PahSettings = {
	SPEED: 3
}

var Pah = cc.Sprite.extend({
	_parentNode:null,

	_idleAnim:null,
	_walkAnim:null,

	currentState: null,
	facingDirection: Direction.LEFT,

	// API Access
	currentFloor: -1,
	BeingEngaged: false,

	initWithStyle:function (parentNode, pahStyle) {
		this._parentNode = parentNode;

		// load kiki spritesheet
		global.loadSpriteFrames(res_miscSpritesheetPlist);

		if(!this.initWithSpriteFrame(global.getSpriteFrame("pah" + pahStyle + "_idle_1.png")))
		{
			global.log("Pah's initWithSpace() error.");
			// unload eloong spritesheet
			global.unloadSpriteFrames(res_miscSpritesheetPlist);

			return false;
		}

		// init stuff
		this.facingDirection = Direction.LEFT;
		this.currentFloor = -1;
		this.BeingEngaged = false;

		// create animations
		var frames = new Array();
		var animate = null;

		// -- idle
		frames.push(global.getSpriteFrame("pah" + pahStyle + "_idle_1.png"));
		frames.push(global.getSpriteFrame("pah" + pahStyle + "_idle_2.png"));
		animate = cc.Animate.create(cc.Animation.create(frames, 1/7.0));
		this._idleAnim = cc.RepeatForever.create(animate);

		// -- walk
		frames.length = 0;
		frames.push(global.getSpriteFrame("pah" + pahStyle + "_walk_1.png"));
		frames.push(global.getSpriteFrame("pah" + pahStyle + "_walk_2.png"));
		animate = cc.Animate.create(cc.Animation.create(frames, 1/7.0));
		this._walkAnim = cc.RepeatForever.create(animate);

		// unload kiki spritesheet
		global.unloadSpriteFrames(res_miscSpritesheetPlist);

		this.playIdleAnimation();

		return true;
	},
	playIdleAnimation:function() {
		if(this.currentState != PahState.IDLE_STATE)
		{
			this.stopAllActions();

			this.currentState = PahState.IDLE_STATE;
			this.runAction(this._idleAnim);
		}
	},
	playWalkAnimation:function() {
		if(this.currentState != PahState.WALK_STATE)
		{
			this.stopAllActions();

			this.currentState = PahState.WALK_STATE;
			this.runAction(this._walkAnim);
		}
	},
	setFacingDirection:function(direction) {
		if(direction == Direction.LEFT)
		{
			this.facingDirection = Direction.LEFT;
			this.setFlipX(false);
		}
		else if(direction == Direction.RIGHT)
		{
			this.facingDirection = Direction.RIGHT;
			this.setFlipX(true);
		}
	}
});

Pah.create = function(parentNode, pahStyle) {
	var pah = new Pah();
	if(pah && pah.initWithStyle(parentNode, pahStyle))
	{
		return pah;
	}
	else
	{
		pah = null;
		return null;
	}
}