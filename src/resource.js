var res_splashLogo = "res/logo.png";

// -- mainMenu scene
var res_gameLogo = "res/title/title_logo.png";
var res_gameTitleBG = "res/title/title_bg.png";

// -- tutorial scene
var res_gameTutorial = "res/tutorial/tutorial.png";

// -- ui
var res_uiBG = "res/ui/ui_bg.png";
var res_UIDefault = "res/ui/ui_default.png";
var res_UIDefaultPlist = "res/ui/ui_default.plist";
// -- texts
var res_uiTextHP = "res/ui/text/hp_text.png";
var res_uiTextStamina = "res/ui/text/stamina_text.png";
// -- hp
var res_gameUIHPBar = "res/ui/hp/hp_bar.png";
var res_gameUIHPBarBG = "res/ui/hp/hp_bg.png";
var res_uiHPSymbol = "res/ui/hp/hp.png";
// -- heart (normal)
var res_gameUIHeart_1 = "res/ui/heart/heart_1.png";
var res_gameUIHeart_2 = "res/ui/heart/heart_2.png";
var res_gameUIHeart_3 = "res/ui/heart/heart_3.png";
// -- heart (tired)
var res_gameUIHeartTired_1 = "res/ui/heart_tired/heart_tired_1.png";
var res_gameUIHeartTired_2 = "res/ui/heart_tired/heart_tired_2.png";
var res_gameUIHeartTired_3 = "res/ui/heart_tired/heart_tired_3.png";
// -- fire on heart
var res_gameUIFireOnHeart = "res/ui/fire_on_heart/fire_on_heart_1.png";
// -- stamina
var res_staminaBar = "res/ui/stamina/stamina_bar.png";
var res_staminaBarTired = "res/ui/stamina/stamina_bar_tired.png";
var res_staminaBarBG = "res/ui/stamina/stamina_bar_bg.png";

var res_creditsLogo = "res/logo-bw.png";

// -- BGM music
var res_music = "res/music.mp3";

// -- sfx
// -- ui
var res_sfxSelect = "res/sfx/ui/select.mp3";
// -- combat
var res_gamePunchLight = "res/sfx/combat/punch_light.mp3";
var res_gamePunchHeavy = "res/sfx/combat/punch_heavy.mp3";
var res_gamePowerUp = "res/sfx/combat/powerup.mp3";
var res_gamePowerDown = "res/sfx/combat/powerdown.mp3";
var res_sfxHit = "res/sfx/combat/Hit.mp3";
var res_sfxKikiHit = "res/sfx/combat/kikiHit.mp3";
var res_sfxKikiDie = "res/sfx/combat/kikiDie.mp3";
// -- movement
var res_sfxJump = "res/sfx/movement/jump.mp3";
var res_sfxLanding = "res/sfx/movement/landing.mp3";


// -- level
var res_levelsSpritesheetPlist = "res/levels/levels_default.plist";
var res_levelsSpritesheet = "res/levels/levels_default.png"

// -- eloong
var res_eloongSpritesheetPlist = "res/eloong/eloong_default.plist";
var res_eloongSpritesheet = "res/eloong/eloong_default.png";

// -- fire on eloong
var res_eloongFire = "res/eloong_fire/eloong_fire_default.png";
var res_eloongFirePlist = "res/eloong_fire/eloong_fire_default.plist";
var res_eloongFire_1 = "res/eloong_fire/eloong_fire_1.png";

// -- miscellenous
var res_miscSpritesheetPlist = "res/misc/misc_default.plist";
var res_miscSpritesheet = "res/misc/misc_default.png";

var g_ressources = [
    //image
    {type:"image", src: res_splashLogo},
    {type:"image", src: res_gameLogo},
    {type:"image", src: res_creditsLogo},
    {type:"image", src: res_levelsSpritesheet},
    {type:"image", src: res_eloongSpritesheet},
    {type:"image", src: res_miscSpritesheet},

    //plist
    {type:"plist", src: res_levelsSpritesheetPlist},
    {type:"plist", src: res_eloongSpritesheetPlist},
    {type:"plist", src: res_miscSpritesheetPlist},
    {type:"plist", src: res_UIDefaultPlist},
    {type:"plist", src: res_eloongFirePlist},

    //fnt

    //tmx

    //bgm
    {type: "bgm", src: res_music},

    //effect
    // {type: "bgm", src: res_music},
];