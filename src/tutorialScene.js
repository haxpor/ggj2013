var tutorialLayer = cc.LayerColor.extend({

	// constants
	_gameTutorial: null,

	init:function (color) {
		if(!this._super(color))
		{
			cc.log("tutorialLayer's init() called failed.");
			return false;
		}

		var winSize = cc.Director.getInstance().getWinSize();

		// -- tutorial image
		this._gameTutorial = cc.Sprite.create(res_gameTutorial);
		this._gameTutorial.setPosition(cc.p(winSize.width/2, winSize.height/2));
		this._gameTutorial.setOpacity(0);
		this._gameTutorial.setScale(2.5);
		this.addChild(this._gameTutorial);

		var fadeInTutorial = cc.FadeTo.create(1,255);
		this._gameTutorial.runAction(fadeInTutorial);

		this.setKeyboardEnabled(true);

		return true;
	},
	onKeyUp:function(e) {

	},
	onKeyDown:function(e) {
        if(e == cc.KEY.x || e == cc.KEY.enter || e == cc.KEY.space)
        {
        	cc.Director.getInstance().replaceScene(cc.TransitionFade.create(1.0, new GameplayScene()));
        	// play sfx Dinggg
			audioEngine.playEffect(res_sfxSelect);
        }
    }
});

var TutorialScene = cc.Scene.extend({
	onEnter:function() {
		this._super();

		var layer = new tutorialLayer();
		layer.init(cc.c4b(0,0,0,0));
		this.addChild(layer);
	}
});