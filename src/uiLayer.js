var UILayer = cc.LayerColor.extend({

	// -- parent
	_parentNode: null,

	// -- stamina
	_stamina: null,
	_staminaDecreasion: 1.5,
	_staminaIncreasion: 1.5,

	// -- hp
	_hp: null,

	// -- damages
	_damage: 15,
	_lessDamage: 6,

	// eloong
	_eloong: null,

	// -- sprite: UI elements
	_uiBG: null,
	_uiHP: null,
	_uiStaminaBarBG: null,
	_uiStaminaBar: null,
	_uiStaminaBarTired: null,

	_hpBar: null,
	_hpBarBG: null,

	// -- sprite: heart
	_heartNormal: null,
	_heartNormalBatch: null,
	_heartTired: null,
	_heartTiredBatch: null,
	_fireOnHeart:null,
	_fireOnHeartBatch:null,

	init:function(color){

		this._super(color);

		var winSize = cc.Director.getInstance().getWinSize();

		// -- add UI bg
		this._uiBG = cc.Sprite.create(res_uiBG);
		this._uiBG.setScale(450, 16);
		this._uiBG.setPosition(cc.p(0, winSize.height));
		this.addChild(this._uiBG);

		// -- add ui texts
		this._uiTextHP = cc.Sprite.create(res_uiTextHP);
		this._uiTextHP.setPosition(cc.p(80, winSize.height-60));
		this.addChild(this._uiTextHP);

		this._uiTextStamina = cc.Sprite.create(res_uiTextStamina);
		this._uiTextStamina.setPosition(cc.p(540, winSize.height-60));
		this.addChild(this._uiTextStamina);

		// -- load UI spriteframecache
		cc.SpriteFrameCache.getInstance().addSpriteFrames(res_UIDefaultPlist);

		// -- add stamina bar BG
		this._uiStaminaBarBG = cc.Sprite.createWithSpriteFrameName("stamina_bar_bg.png");
		this._uiStaminaBarBG.setAnchorPoint(cc.p(0,0.5));
		this._uiStaminaBarBG.setPosition(cc.p(440, winSize.height-40));
		this.addChild(this._uiStaminaBarBG);

		// -- add stamina bar
		this._uiStaminaBar = cc.Sprite.createWithSpriteFrameName("stamina_bar.png");
		this._uiStaminaBar.setAnchorPoint(cc.p(0,0.5));
		this._uiStaminaBar.setPosition(cc.p(440, winSize.height-40));
		// -- make it pink!
		var makePink = cc.TintTo.create(0.1,255, 64, 140);
		// this._uiStaminaBar.runAction(makePink);
		this.addChild(this._uiStaminaBar);

		// -- add stamina bar (tired)
		this._uiStaminaBarTired = cc.Sprite.createWithSpriteFrameName("stamina_bar_tired.png");
		this._uiStaminaBarTired.setAnchorPoint(cc.p(0,0.5));
		this._uiStaminaBarTired.setPosition(cc.p(440, winSize.height-40));
		this._uiStaminaBarTired.setOpacity(0);
		this.addChild(this._uiStaminaBarTired);

		// -- fire on heart
		this._fireOnHeart = cc.Sprite.createWithSpriteFrameName("fire_on_heart_1.png");
		this._fireOnHeart.setPosition(cc.p(440, winSize.height-40));
		this._fireOnHeart.setScale(2);
		this._fireOnHeart.setOpacity(0);
		this.addChild(this._fireOnHeart);
		// -- fire on heart
		this._fireOnHeartBatch = cc.SpriteBatchNode.create(res_gameUIFireOnHeart);
		this.addChild(this._fireOnHeartBatch);

		// -- heart normal Sprite
		this._heartNormal = cc.Sprite.createWithSpriteFrameName("heart_1.png");
		this._heartNormal.setPosition(cc.p(440, winSize.height-40));
		this._heartNormal.setScale(2);
		this.addChild(this._heartNormal);
		// -- heart normal batchnode
		this._heartBatch = cc.SpriteBatchNode.create("res/ui/heart/heart_1.png");
		this.addChild(this._heartBatch);

		// -- heart tired
		this._heartTired = cc.Sprite.createWithSpriteFrameName("heart_tired_1.png");
		this._heartTired.setPosition(cc.p(440, winSize.height-40));
		this._heartTired.setScale(2);
		this._heartTired.setOpacity(0);
		
		// -- heart tired batchnode
		this._heartTiredBatch = cc.SpriteBatchNode.create("res/ui/heart_tired/heart_tired_1.png");
		// this._heartTiredBatch.addChild(this._heartTired);
		this.addChild(this._heartTiredBatch);

		// -- prepare frame variables
		var heartFrames = [];
		var heartTiredFrames = [];
		var fireFrames = [];
		var heart_animate = null;

		// -- fire on heart
		fireFrames.push(cc.SpriteFrameCache.getInstance().getSpriteFrame("fire_on_heart_1.png"));
		fireFrames.push(cc.SpriteFrameCache.getInstance().getSpriteFrame("fire_on_heart_2.png"));
		fireFrames.push(cc.SpriteFrameCache.getInstance().getSpriteFrame("fire_on_heart_3.png"));
		fireFrames.push(cc.SpriteFrameCache.getInstance().getSpriteFrame("fire_on_heart_2.png"));

		// -- heart (normal)
		heartFrames.push(cc.SpriteFrameCache.getInstance().getSpriteFrame("heart_1.png"));
		heartFrames.push(cc.SpriteFrameCache.getInstance().getSpriteFrame("heart_2.png"));
		heartFrames.push(cc.SpriteFrameCache.getInstance().getSpriteFrame("heart_3.png"));
		heartFrames.push(cc.SpriteFrameCache.getInstance().getSpriteFrame("heart_2.png"));

		// -- heart (tired)
		heartTiredFrames.push(cc.SpriteFrameCache.getInstance().getSpriteFrame("heart_tired_1.png"));
		heartTiredFrames.push(cc.SpriteFrameCache.getInstance().getSpriteFrame("heart_tired_2.png"));
		heartTiredFrames.push(cc.SpriteFrameCache.getInstance().getSpriteFrame("heart_tired_3.png"));
		heartTiredFrames.push(cc.SpriteFrameCache.getInstance().getSpriteFrame("heart_tired_2.png"));

		// -- create animation
		var fire_animation = cc.Animation.create(fireFrames, 1/5);
		var heart_animation = cc.Animation.create(heartFrames, 1/5);
		var heart_tired_animation = cc.Animation.create(heartTiredFrames, 1/5);

        this._fireOnHeart.runAction(cc.RepeatForever.create(cc.Animate.create(fire_animation)));
        this._heartNormal.runAction(cc.RepeatForever.create(cc.Animate.create(heart_animation)));
        this._heartTired.runAction(cc.RepeatForever.create(cc.Animate.create(heart_tired_animation)));

        // -- add HP bar BG
		this._hpBarBG = cc.Sprite.createWithSpriteFrameName("stamina_bar_bg.png");
		this._hpBarBG.setAnchorPoint(cc.p(0.01,0.5));
		this._hpBarBG.setPosition(cc.p(50, winSize.height-40));
		this.addChild(this._hpBarBG);

        // -- add HP bar
		this._hpBar = cc.Sprite.createWithSpriteFrameName("stamina_bar.png");
		this._hpBar.setPosition(cc.p(50, winSize.height-40));
		this._hpBar.setScale(2);
		this._hpBar.setAnchorPoint(cc.p(0.01,0.5));
		this.addChild(this._hpBar);

		// -- add hp symbol
		this._hpSymbol = cc.Sprite.create(res_uiHPSymbol);
		this._hpSymbol.setPosition(cc.p(50, winSize.height-40));
		this.addChild(this._hpSymbol);

		// -- remove spriteframecache
        cc.SpriteFrameCache.getInstance().removeUnusedSpriteFrames;

		// -- set UI once
		this.resetUI();

		// -- enable keyboards
		this.setKeyboardEnabled(true);

	},
	initWithParentNode:function (parentNode) {

		// -- get parent's elements
		this._parentNode = parentNode;

		if(!this.init(cc.c4b(0,0,0,0)));
		{
			cc.log("UILayer's init() called failed.");
			return false;
		}

		return true;
	},

	// -----------------------
	// Damage functions
	// -----------------------
	getDamage: function(){
		this._hp -= this._damage;
		if(this._hp <= 0)
		{
			// Eloong dies
			this.die();
		}
	},
	getLessDamage: function(){
		this._hp -= this._lessDamage;
		if(this._hp <= 0) this.die();
	},
	die: function(){
		this._hp = 0;
		this._uiStaminaBar.setOpacity(0);
		this._heartNormal.setOpacity(0);
		this._heartTired.setOpacity(0);
		this._uiStaminaBarTired.setOpacity(0);

		// play die animation of Eloong
		this._parentNode._eloong.playDieAnimation();

		// game over
		this._parentNode.isGameOver = true;
		// TODO: transition to the result scene here ...
		global.log("game over");
	},
	wasHitByKiki:function(){

		// easier please
		var currentState = this._parentNode._eloong.currentState;
		// console.log(this._parentNode._eloong.currentState);

		switch(currentState) {
			case EloongState.IDLE_NORMAL_STATE:	this.getDamage(); break;
			case EloongState.RUN_NORMAL_STATE:	this.getDamage(); break;
			case EloongState.ATTACK_NORMAL_STATE:	this.getDamage(); break;
			case EloongState.DIE_STATE:	this.getDamage(); break;
			case EloongState.BEING_ATTACK_STATE:	this.getDamage(); break;
			case EloongState.JUMP_NORMAL_STATE:	this.getDamage(); break;

			case EloongState.IDLE_HYPER_STATE:	this.getDamage(); break;
			case EloongState.RUN_HYPER_STATE:	this.getDamage(); break;
			case EloongState.ATTACK_HYPER_STATE:	this.getDamage(); break;
			case EloongState.ATTACKRUN_HYPER_STATE:	this.getDamage(); break;
			case EloongState.JUMP_HYPER_STATE:	this.getDamage(); break;

			case EloongState.IDLE_TIRED_STATE:	this.getDamage(); break;
			case EloongState.RUN_TIRED_STATE:	this.getDamage(); break;
			case EloongState.ATTACK_TIRED_STATE:	this.getDamage(); break;
			case EloongState.JUMP_TIRED_STATE:	this.getDamage(); break;
		}

		// -- update UI
		this.updateUI();

	},

	// -----------------------
	// Stamina functions
	// -----------------------
	decreaseStamina: function(){
		this._stamina -= this._staminaDecreasion;
		this.updateUI();
	},
	increaseStamina: function() {
		this._stamina += this._staminaDecreasion;
		this.updateUI();
	},
	enterHyperMode:function(){
		this._fireOnHeart.setOpacity(255);
	},
	exitHyperMode:function(){
		this._fireOnHeart.setOpacity(0);
	},
	toggleHyperMode:function(){
		if(this._parentNode._eloong.currentState >= EloongState.IDLE_HYPER_STATE && this._parentNode._eloong.currentState <= EloongState.JUMP_HYPER_STATE)
		{
			this.enterHyperMode();
		} else {
			this.exitHyperMode();
		}
		global.log(this._parentNode._eloong.currentState);
	},

    // -----------------------
	// Reset functions
	// -----------------------
	resetUI: function(){
		// -- reset variables
		this._hp = 100;
		this._stamina = 100;
		// -- reset sprites
		this._uiStaminaBar.setOpacity(255);
		this._heartNormal.setOpacity(255);
		this._heartTired.setOpacity(0);
		this._uiStaminaBarTired.setOpacity(0);
		// this._fireOnHeart.setOpacity(0);
		// -- update UI
		this.updateUI();
	},

	// -----------------------
	// Display functions
	// -----------------------
	updateUI: function(){ 

		// -- hp
		if(this._hp <= 0 || this._hp == null || this._hp == NaN) {
			this._hp = 0;
		} else if(this._hp >= 100) {
			this._hp = 100;
		}

		// update HP
		this._hpBar.setScale( (this._hp/100), 1);

		// -- stamina
		if(this._stamina <= 0 || this._stamina == null || this._stamina == NaN) {
			this._stamina = 0;
		} else if(this._stamina >= 100) {
			this._stamina = 100;
		} else {
			// -- update stamina
			// -- decide which one to show
			if(this._stamina <= 30) {
				// -- tired!
				this._uiStaminaBar.setOpacity(0);
				this._heartNormal.setOpacity(0);
				this._heartTired.setOpacity(255);
				this._uiStaminaBarTired.setOpacity(255);
			} else {
				// -- not tired, ok
				this._uiStaminaBar.setOpacity(255);
				this._heartNormal.setOpacity(255);
				this._heartTired.setOpacity(0);
				this._uiStaminaBarTired.setOpacity(0);
			}
			// -- set its scale
			var staminaBarScaleSize = this._stamina/100;
			this._uiStaminaBar.setScale( staminaBarScaleSize, 1);
			this._uiStaminaBarTired.setScale( staminaBarScaleSize, 1);
		}


	},

	// -----------------------
	// Input functions
	// -----------------------
	onKeyUp:function(e) {
		// nothing
	},
	onKeyDown:function(e) {
		// simulate hit by Kiki
		/*if(e == cc.KEY.k) {
			this.wasHitByKiki();
		}
		if(e == cc.KEY.l) {
			this.resetUI();
		}
		if(e == cc.KEY.o) {
			this.decreaseStamina();
		}
		if(e == cc.KEY.p) {
			this.increaseStamina();
		}
		if(e == cc.KEY.c) {
			this.toggleHyperMode();
		}*/
    },

});